<?php

/**
* @package	Routes
*/

return [

	'/'			=> "Home",
	"controls"	=> "ControlsController",

	# Project Kiba
	"kiba"				=> "kiba:Kiba",
	"kiba/simpledb"		=> "kiba:SimpleDBController",

];