<?php

/**
* @package	CSV
* @version	1.6
* @author	DavidBeru
* @since	2014-04-19
* @see		2015-07-07
*/

class CSV
{
	public static function Make($_data = null)
	{
		if (is_array($_data))
		{
			$row = null;

			foreach ($_data as $rkey => $value)
			{
				$col = null;

				if (is_array($value))
				{
					foreach ($value as $ckey => $sub_value)
					{
						if (is_array($sub_value))
						{
							$comma = null;

							foreach ($sub_value as $skey => $svalue)
							{
								$comma .= utf8_decode($svalue) . ', ';
							}

							$col .= '"' . substr($comma, 0, -2) . '",';
						}
						else
						{
							$col .= '"' . utf8_decode($sub_value) . '",';
						}
					}

					$row .= substr($col, 0, -1) . "\r\n";
				}
				else
				{
					#$row .= "\r\n";
				}
			}

			return substr($row, 0, -2);
		}
		else
		{
			return false;
		}
	}

	public static function Interpret($_content = null)
	{
		if ($_content)
		{
			$values = String::Split($_content, ',');
			$cols = 0;
			$rows = 0;
			$columns = true;
			$data = array();
			$i = 0;

			foreach ($values as $key => $value)
			{
				$value = String::Split($value, "\n");

				if ($columns)
				{
					$cols++;
				}

				#if (!empty($value[0]))
				#{
					$data[$i][] = $value[0];
				#}

				if (isset($value[1]))
				{
					$rows++;
					$columns = false;
					$i++;

					#if (!empty($value[1]))
					#{
						$data[$i][] = $value[1];
					#}
				}
			}

			return array(

				"error" => false,
				"info" => array(
					"cols" => $cols,
					"rows" => $rows),
				"data" => $data

			);
		}
		else
		{
			return array("error" => "content empty");
		}
	}
}