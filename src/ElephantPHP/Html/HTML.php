<?php

/**
* @package	HTML
* @version	3.2
* @author	DavidBeru
* @since	2013-07-22
* @see		2015-10-19
*/

class HTML
{
	public static function Open($_tag = null, $_value = null, $_attr = array(), $_close = true)
	{
		$html = new HTMLi();
		$html->Open($_tag, $_value, $_attr, $_close);

		return $html;
	}

	public static function Close($_tag = null)
	{
		$html = new HTMLi();

		return $html->Close($_tag);
	}

	public static function BreadCrumbs($_class = "breadcrumbs")
	{
		$html = new HTMLi();

		return $html->BreadCrumbs($_class);
	}

	public static function Link($_link = null, $_value = null, $_target = null)
	{
		$html = new HTMLi();
		$html->Open('a', $_value);
		$html->Attr("href", $_link);
		$html->Attr("target", $_target);

		return $html;
	}

	public static function Img($_src = null, $_alt = null)
	{
		$html = new HTMLi();
		$html->Open("img", $_src);
		$html->Attr("alt", $_alt);

		return $html;
	}
}