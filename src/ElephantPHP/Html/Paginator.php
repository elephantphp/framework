<?php

/**
* @package	Paginator
* @version	2.0
* @author	DavidBeru
* @since	2013-09-13
* @see		2015-12-29
*/

class Paginator
{
	public static function Ajax($_control = null, $_name = null, $_num = 1, $_perpage = 10, $domain = null)
	{
		if ($_num > 1)
		{
			$page  = Request::Post("elephant_control_page", 1);
			$perpage = ($_perpage == 0) ? 1 : $_perpage;
			$total = ($_num === 0) ? 0 : ceil($_num / $perpage);
			$prev = $page - 1; 
			$next = $page + 1;
			$prev = ($page < 2) ? 1 : $prev;
			$next = (($page + 1) > $total) ? $total : $next;

			if ($total == 1)
			{
				return null;
			}
			else
			{
				return View::Make("@{base}.elephant.views.paginator.bootstrap.paginator", array(

					"control" => $_control,
					"name" => $_name,
					"perpage" => $perpage,
					"prev" => $prev,
					"page" => $page,
					"total" => $total,
					"next" => $next

				));
			}
		}
		else
		{
			return null;
		}
	}

	public static function Input($_num = 1, $_perpage = 10, $domain = null)
	{
		if ($_num > 1)
		{
			$page = isset($_GET["page"]) ?$_GET["page"]: 1;
			$total = ceil($_num / $_perpage);
			$prev = $page - 1; 
			$next = $page + 1;
			$prev = ($page < 2) ? 1 : $prev;
			$next = (($page + 1) > $total) ? $total : $next;
			$first = null;
			$before = null;
			$after = null;
			$last = null;
			$li = null;
			$html = new HTML();

			# First
			if ($page > 2)
			{
				$a = $html->Open("a", "Primero", array("href" => "{$domain}?page=1"));
				$first = $html->Open("li", $a, array("title" => "Ir a la primera página"));
			}

			# Before
			if ($page > 1)
			{
				$a = $html->Open("a", "Anterior", array("href" => "{$domain}?page={$prev}"));
				$before = $html->Open("li", $a, array("title" => "Página anterior"));
			}

			# After
			if ($page < $total)
			{
				$a = $html->Open("a", "Siguiente", array("href" => "{$domain}?page={$next}"));
				$after = $html->Open("li", $a, array("title" => "Página siguiente"));
			}

			# Last
			if ($page < ($total - 1))
			{
				$a = $html->Open("a", "Último", array("href" => "{$domain}?page={$total}"));
				$last = $html->Open("li", $a, array("title" => "Ir a la ultima página"));
			}

			$input = Form::Text("page", $page, array("class" => "paginator"));
			$form = Form::Open($domain, "GET") . $input . Form::Close();
			$li .= $html->Open("li", $form, array("title" => "Página actual"));
			$a = $html->Open("a", "de {$total} páginas", array("href" => "{$domain}?page={$total}"));
			$li .= $html->Open("li", $a, array("title" => "Total de páginas"));

			return $html->Open("ul", "{$first}{$before}{$li}{$after}{$last}", array(
				"class" => "paginator"));
		}
		else
		{
			return null;
		}
	}
}