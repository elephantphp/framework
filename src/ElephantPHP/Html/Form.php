<?php

/**
* @package	Form
* @version	4.10
* @author	DavidBeru
* @since	2013-07-23
* @see		2016-01-09
*/

class Form
{
	private static function Name($_name = null)
	{
		$split = String::Split($_name, "[]");

		return $split[0];
	}

	private static function Input($_type = null, $_name = null, $_value = null, $_attr = array())
	{
		$id = String::Split($_name, '[');
		$id = $id[0];
		$attr = is_array($_value) ? $_value : $_attr;
		$attr = array_merge(
			$attr,
			array(
				"type" => $_type,
				"name" => $_name,
				"id" => $id,
				"autocomplete" => "off"));
		$value = is_array($_value) ? null : $_value;
		$value = ($_type == "radio" || $_type == "checkbox") ? $value : self::Val($_name, $value);

		return HTML::Open("input", $value, $attr);
	}

	public static function Val($_name = null, $_value = null)
	{
		$name = self::Name($_name);

		return Request::Post($name, Request::Get($name, $_value));
	}

	public static function Open($_action = null, $_method = "POST", $_attr = array(), $_close = false)
	{
		$action = URI::Interpret($_action);
		$method = Fn::Upper($_method);
		$method = ($method == "POST" || $method == "GET") ? $method : "POST";

		return HTML::Open("form", null, array_merge(
			array(
				"action" => $action,
				"method" => $method,
				"accept-charset" => "UTF-8"),
			$_attr), $_close);
	}

	public static function OpenForFiles($_action = null, $_attr = array())
	{
		$_attr["enctype"] = "multipart/form-data";

		return self::Open($_action, "POST", $_attr);
	}

	public static function Close()
	{
		return HTML::Close("form");
	}

	public static function Range($_name = null, $_start = 0, $_end = 0, $_selected = 0, $_attr = array())
	{
		$values = array();

		for ($i = $_start; $i <= $_end; $i++)
		{
			$values[$i] = $i;
		}

		return self::Select($_name, $values, $_selected, $_attr);
	}

	public static function Label($_for = null, $_text = null, $_attr = array())
	{
		$_attr["for"] = $_for;

		return HTML::Open("label", $_text, $_attr);
	}

	public static function Text($_name = null, $_value = null, $_attr = array())
	{
		return self::Input("text", $_name, $_value, $_attr);
	}

	public static function CURP($_name = null, $_value = null, $_attr = array())
	{
		$_attr["maxlength"] = 18;

		return self::Text($_name, $_value, $_attr);
	}

	public static function CP($_name = null, $_value = null, $_attr = array())
	{
		$_attr["maxlength"] = 5;

		return self::Text($_name, $_value, $_attr);
	}

	public static function Phone($_name = null, $_value = null, $_attr = array())
	{
		$_attr["maxlength"] = 10;

		return self::Text($_name, $_value, $_attr);
	}

	public static function Search($_name = null, $_value = null, $_attr = array())
	{
		return self::Input("search", $_name, $_value, $_attr);
	}

	public static function Hidden($_name = null, $_value = null, $_attr = array())
	{
		return self::Input("hidden", $_name, $_value, $_attr);
	}

	public static function Date($_name = null, $_value = null, $_attr = array())
	{
		return self::Input("date", $_name, $_value, $_attr);
	}

	public static function Number($_name = null, $_value = 0, $_attr = array())
	{
		return self::Input("number", $_name, $_value, $_attr);
	}

	public static function File($_name = null, $_value = null, $_attr = array())
	{
		return self::Input("file", $_name, $_value, $_attr);
	}

	public static function Password($_name = null, $_value = null, $_attr = array())
	{
		return self::Input("password", $_name, $_value, $_attr);
	}

	public static function TextArea($_name = null, $_value = null, $_attr = array())
	{
		$_attr["name"] = $_name;
		$_attr["id"] = $_name;

		return HTML::Open("textarea", self::Val($_name, $_value), $_attr);
	}

	public static function Submit($_name = null, $_value = null, $_attr = array())
	{
		$_attr["type"] = "submit";
		$_attr["name"] = $_name;
		$_attr["id"] = $_name;
		$_attr["value"] = $_value;

		return HTML::Open("button", self::Val($_name, $_value), $_attr);
	}

	public static function Button($_name = null, $_value = null, $_attr = array())
	{
		$_attr["type"] = "button";
		$_attr["name"] = $_name;
		$_attr["id"] = $_name;
		$_attr["value"] = $_value;

		return HTML::Open("button", self::Val($_name, $_value), $_attr);
	}

	public static function Select($_name = null, $_options = null, $_selected = null, $_attr = null)
	{
		if ($_name)
		{
			$options = HTML::Open("option", null, array("value" => null));
			$selected = self::Val($_name) ? self::Val($_name) : $_selected;

			if (is_array($_options))
			{
				foreach ($_options as $key => $value)
				{
					if (is_array($value))
					{
						$optgroup = null;

						foreach ($value as $keyin => $valuein)
						{
							$optgroup .= "<option value=\"{$keyin}\"" . Fn::Equal($keyin, $selected, " selected=\"selected\"") . ">{$valuein}</option>";
						}

						$options .= "<optgroup label=\"" . $key . "\">" . $optgroup . "</optgroup>";
					}
					else
					{
						$options .= "<option value=\"{$key}\"" . Fn::Equal($key, $selected, " selected=\"selected\"") . ">{$value}</option>";
					}
				}
			}
			else
			{
				$control = Model::Get($_options, "select");

				if ($control)
				{
					$cl = $control["control"];
					$sql = Fn::Args($cl["sql"], $control["args"]);
					$fields = $cl["fields"];
					$db = isset($cl["db"]) ? $cl["db"] : null;
					$data = DB::Select($sql, $db);

					if ($data->Rows())
					{
						while ($row = $data->Result())
						{
							$options .= "<option value=\"" . $row[$fields[0]] . '"' . Fn::Equal($row[$fields[0]], $selected, " selected=\"selected\"") . '>' . $row[$fields[1]] . "</option>";
						}
					}
				}
			}

			$_attr["name"] = $_name;
			$_attr["id"] = $_name;

			return HTML::Open("select", $options, $_attr);
		}
	}

	public static function CheckBox($_name = null, $_value = null, $_checked = array(), $_attr = array())
	{
		if (is_array($_value))
		{
			$checked = self::Val($_name) ? self::Val($_name) : $_checked;

			if (is_array($checked))
			{
				$html = null;

				foreach ($_value as $key => $value)
				{
					$attr = in_array($key, $checked) ? array_merge(array("checked"), $_attr) : $_attr;
					$html .= "<p>" . self::Input("checkbox", $_name, $key, $attr) . " {$value}</p>";
				}

				return $html;
			}
			else
			{
				return "CheckBox checked should be a array.";
			}
		}
		else
		{
			return "CheckBox value should be a array.";
		}
	}

	public static function Radio($_name = null, $_value = null, $_checked = null, $_attr = array())
	{
		$radio = null;

		if (is_array($_value))
		{
			foreach ($_value as $key => $value)
			{
				$radio .= "<label>" . self::Radio($_name, $key, self::Val($_name, $_checked), $_attr) . $value . "</label>";
			}
		}
		else
		{
			$attr = ($_value == self::Val($_name, $_checked)) ? array_merge(array("checked"), $_attr) : $_attr;
			$radio = self::Input("radio", $_name, $_value, $attr);
		}

		return $radio;
	}

	public static function Assess($_name = null, $_options = null, $_selected = null, $_attr = null)
	{
		$radio = null;

		if (is_array($_options))
		{
			$nivel = 0;

			foreach ($_options as $id => $valor)
			{
				$options = "<option value=\"\"></option>";

				for ($i = 0; $i < count($_options); $i ++)
				{
					$options .= '<option value="' . $i . '_' . $id . '"' . (($i === @array_search($id, $_selected)) ? " selected=\"selected\"" : null) .'>' . ($i + 1) . '</option>';
				}

				$radio .= "<label class=\"input-assess\">" . HTML::Open("select", $options)->Attr("name", "{$_name}[]")->Attr("id", $_name) . "<span>" . $valor . "</span></label>";
				$options = null;
				$nivel ++;
			}
		}

		return $radio;
	}

	public static function MediaUpload($_name = null, $_image = null, $_attr = array())
	{
		$_attr["data-fw-control-type"] = "mediaupload";
		$_attr["data-fw-control-name"] = $_name;
		$preview = null;
		$img = null;
		$html = self::OpenForFiles();
		$html .= self::Hidden($_name, $_image);

		if (!empty($_image))
		{
			$preview = HTML::Open("img", $_image);
			$img = " min";
		}

		$area = HTML::Open("div", $preview, array("class" => "vista-previa"));
		$media = self::File("media-upload", null, $_attr);
		$media .= self::Label("media-upload", "Haz clic aquí para seleccionar archivo.");
		$area .= HTML::Open("div", $media, array("class" => "area-upload-select"));
		$html .= HTML::Open("div", $area, array("class" => "area-upload"));
		$html .= self::Close();

		return $html;
	}
}