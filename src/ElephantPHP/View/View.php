<?php

/**
* @package	View
* @version	2.10
* @author	DavidBeru
* @since	2014-01-20
* @see		2016-01-09
*/

class View
{
	private static $view = null;
	private static $path = null;
	private static $vars = array();
	private static $regex_this = '/@\{this\}/i';
	private static $regex_controller = '/@\{controller\}/i';
	private static $regex_method = '/@\{method\}/i';

	private static function GetContent()
	{
		ob_start();

		if (is_array(self::$vars) && isset(self::$vars[self::$view]))
		{
			$elephant_view_vars = array();

			foreach (self::$vars[self::$view] as $key => $value)
			{
				$elephant_view_vars[] = $key;
				$$key = $value;
			}
		}

		include self::$path;

		return ob_get_clean();
	}

	private static function Assign($_key = null, $_value = null)
	{
		self::$vars[self::$view][$_key] = $_value;
	}

	private static function GetMethodName()
	{
		$method = ElephantPHP::GetMethodName();

		return str_replace("__", '/', $method);
	}

	public static function Exists($_view = null)
	{
		if (is_null($_view))
		{
			return null;
		}
		else
		{
			$view = preg_replace_callback(self::$regex_this, function($matches)
			{
				$val = null;

				foreach ($matches as $key => $value)
				{
					if (preg_match(self::$regex_this, $value))
					{
						$controller = ElephantPHP::GetControllerName();
						$method = self::GetMethodName();
						$val = "{$controller}.{$method}";
					}
				}

				return $val;

			}, $_view);

			$view = preg_replace_callback(self::$regex_controller, function($matches)
			{
				$val = null;

				foreach ($matches as $key => $value)
				{
					if (preg_match(self::$regex_controller, $value))
					{
						$val = ElephantPHP::GetControllerName();
					}
				}

				return $val;

			}, $view);

			$view = preg_replace_callback(self::$regex_method, function($matches)
			{
				$val = null;

				foreach ($matches as $key => $value)
				{
					if (preg_match(self::$regex_method, $value))
					{
						$val = self::GetMethodName();
					}
				}

				return $val;

			}, $view);

			$view = str_replace('.', '/', String::Lower($view)) . ".php";
			$split = String::Split($view, "@{base}/");
			self::$view = (count($split) > 1) ? $split[1] : "app/views/" . $split[0];
			self::$path = ELEPHANTPHP_BASE . self::$view;

			return file_exists(self::$path) ? true : false;
		}
	}

	public static function Make($_view = null, $_assign = array())
	{
		if (is_array($_view))
		{
			$_assign = $_view;
			$_view = null;
		}

		$view = is_null($_view) ? ElephantPHP::GetControllerPath() . ElephantPHP::GetControllerName() . '.' . self::GetMethodName() : $_view;

		if (is_null($view))
		{
			Response::Error(700, "The route to the view is empty.");
		}
		else
		{
			$exists = self::Exists($view);

			if ($exists === true)
			{
				if (is_array($_assign))
				{
					foreach ($_assign as $key => $value)
					{
						self::Assign($key, $value);
					}
				}

				return self::GetContent();
			}
			else
			{
				Response::Error(
					700,
					"The view was not found: \"" . self::$view . "\"",
					null,
					null,
					ELEPHANTPHP_LINK_DOC_ERRORS);
			}
		}
	}

	public static function Receive($_var = null)
	{
		return isset($$_var) ? $$_var : null;
	}
}