<?php

/**
* @package	String
* @version	2.11
* @author	DavidBeru
* @since	2013-07-26
* @see		2016-01-05
*/

class String
{
	public static function Markup($_subject = null, $_search = null, $_replace = "text-markup")
	{
		if ($_search != null)
		{
			$n = array(' ', 'ú', 'Ú');
			$y = array('', 'u', 'U');

			#$_subject = str_ireplace($n, $y, $_subject);
			$_search = str_ireplace($n, $y, $_search);

			return preg_replace("/(" . $_search . ")/i", "<span class=\"" . $_replace . "\">" . '${1}' . "</span>", $_subject);
		}
		else
		{
			return $_subject;
		}
	}

	public static function ClearAccents($_string = null)
	{
		$find = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
		$replace = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";

		return strtr(utf8_decode($_string), utf8_decode($find), $replace);
	}

	public static function ClearHTML($_string = null)
	{
		$find = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s');
		$replace = array('>', '<', '\\1');
		$string = preg_replace($find, $replace, $_string);
		$string = str_replace("> <", "><", $string);

		return $string;
	}

	public static function Lower($_string = null)
	{
		if (is_string($_string))
		{
			return strtolower(strtr($_string, "ÁÉÍÓÚÑ", "áéíóúñ"));
		}
		else
		{
			return $_string;
		}
	}

	public static function Upper($_string = null)
	{
		if (is_string($_string))
		{
			return strtoupper(strtr($_string, "áéíóúñ", "ÁÉÍÓÚÑ"));
		}
		else
		{
			return $_string;
		}
	}

	public static function Capitalize($_string = null)
	{
		if (is_string($_string))
		{
			return ucfirst(self::Lower($_string));
		}
		else
		{
			return $_string;
		}
	}

	public static function CapitalizeWords($_string = null)
	{
		if (is_string($_string))
		{
			return ucwords(self::Lower($_string));
		}
		else
		{
			return $_string;
		}
	}

	public static function SpecialChar($_string = null)
	{
		$string = str_replace('<', '&lt;', $_string);
		$string = str_replace('>', '&gt;', $string);
		$string = str_replace('"', '&quot;', $string);

		return $string;
	}

	public static function Split($_string = null, $_separator = ',')
	{
		return explode($_separator, $_string);
	}

	public static function GetText($_string = null)
	{
		if (is_string($_string))
		{
			return strip_tags($_string);
		}
		else
		{
			return $_string;
		}
	}

	public static function Trim($_value = null)
	{
		if (is_string($_value))
		{
			return trim($_value);
		}
		else if (is_array($_value))
		{
			return array_map('trim', $_value);
		}
		else
		{
			return $_value;
		}
	}

	public static function MoneyFormat($format = null, $number = null) 
	{ 
		$regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?' . '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/'; 

		if (setlocale(LC_MONETARY, 0) == 'C')
		{
			setlocale(LC_MONETARY, '');
		}

		$locale = localeconv();
		preg_match_all($regex, $format, $matches, PREG_SET_ORDER);

		foreach ($matches as $fmatch)
		{
			$value = floatval($number);
			$flags = array(

				'fillchar'	=> preg_match('/\=(.)/', $fmatch[1], $match) ? $match[1] : ' ',
				'nogroup'	=> preg_match('/\^/', $fmatch[1]) > 0,
				'usesignal'	=> preg_match('/\+|\(/', $fmatch[1], $match) ? $match[0] : '+',
				'nosimbol'	=> preg_match('/\!/', $fmatch[1]) > 0,
				'isleft'	=> preg_match('/\-/', $fmatch[1]) > 0

			); 
			$width		= trim($fmatch[2]) ? (int)$fmatch[2] : 0;
			$left 		= trim($fmatch[3]) ? (int)$fmatch[3] : 0;
			$right 		= trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
			$conversion	= $fmatch[5];
			$positive = true;

			if ($value < 0)
			{
				$positive = false; 
				$value  *= -1; 
			}

			$letter = $positive ? 'p' : 'n';
			$prefix = $suffix = $cprefix = $csuffix = $signal = '';
			$signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];

			switch (true)
			{
				case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
					$prefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
					$suffix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
					$cprefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
					$csuffix = $signal;
					break;
				case $flags['usesignal'] == '(':
				case $locale["{$letter}_sign_posn"] == 0:
					$prefix = '(';
					$suffix = ')';
					break;
			}

			if (!$flags['nosimbol'])
			{
				$currency = $cprefix . ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) . $csuffix;
			}
			else
			{
				$currency = '';
			}

			$space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';
			$value = number_format($value, $right, $locale['mon_decimal_point'], $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
			$value = @explode($locale['mon_decimal_point'], $value);
			$n = strlen($prefix) + strlen($currency) + strlen($value[0]);

			if ($left > 0 && $left > $n)
			{
				$value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
			}

			$value = implode($locale['mon_decimal_point'], $value);

			if ($locale["{$letter}_cs_precedes"])
			{
				$value = $prefix . $currency . $space . $value . $suffix;
			}
			else
			{
				$value = $prefix . $value . $space . $currency . $suffix;
			}

			if ($width > 0)
			{
				$value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ? STR_PAD_RIGHT : STR_PAD_LEFT);
			} 

			$format = str_replace($fmatch[0], $value, $format);
		}

		return $format;
	}
}