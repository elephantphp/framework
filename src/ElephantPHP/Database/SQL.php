<?php

/**
* @package	SQL
* @version	2.6.1
* @author	DavidBeru
* @since	2013-07-19
* @see		2015-06-22
*/

class SQL
{
	private static $regex_null = '/@\{null\}/i';
	private static $regex_current = '/@\{current\}/i';

	private static function FieldType($_field = null)
	{
		$field_split = String::Split($_field, ':');
		$field_count = count($field_split);

		if ($field_count > 1)
		{
			return array("field" => $field_split[0], "type" => $field_split[1]);
		}
		else
		{
			return array("field" => $field_split[0], "type" => null);
		}
	}

	private static function NoSQLInjection($_type = null, $_input = null)
	{
		if ($_type === "sql")
		{
			return '' . $_input . ',';
		}
		else if (preg_match(self::$regex_null, $_input))
		{
			return "null,";
		}
		else if (is_null($_input))
		{
			return "null,";
		}
		else if (Validator::Int($_input))
		{
			return '"' . $_input . '",';
		}
		else if (is_string($_input))
		{
			return '"' . html_entity_decode(addslashes($_input)) . '",';
		}
		else
		{
			return "null,";
		}
	}

	public static function Select($_sql = null)
	{
		return trim($_sql);
	}

	public static function Insert($_sql = null, $_table = null)
	{
		if ($_table && $_sql)
		{
			$fields = null;
			$values = null;

			foreach ($_sql as $key => $value)
			{
				if (!preg_match(self::$regex_current, $value))
				{
					$field_type = self::FieldType($key);
					$fields .= "{$field_type["field"]},";
					$values .= self::NoSQLInjection($field_type["type"], $value);
				}
			}

			$fields = substr($fields, 0, -1);
			$values = substr($values, 0, -1);

			return "INSERT INTO {$_table} ({$fields}) VALUES ({$values})";
		}
	}

	public static function Update($_sql = null, $_table = null, $_nonull = false)
	{
		if ($_table && $_sql)
		{
			$fields = null;

			foreach ($_sql as $key => $value)
			{
				if ($_nonull === true)
				{
					if (is_null($value))
					{
						$fields .= null;
					}
					else
					{
						$field_type = self::FieldType($key);
						$fields .= " {$field_type["field"]} = " . self::NoSQLInjection($field_type["type"], $value);
						#$fields .= " {$key} = " . self::NoSQLInjection($value);
					}
				}
				else
				{
					if (!preg_match(self::$regex_current, $value))
					{
						$field_type = self::FieldType($key);
						$fields .= " {$field_type["field"]} = " . self::NoSQLInjection($field_type["type"], $value);
						#$fields .= " {$key} = " . self::NoSQLInjection($value);
					}
				}
			}

			if (is_null($fields))
			{
				return false;
			}
			else
			{
				$fields = substr($fields, 0, -1);
				$pos = strpos(String::Lower($_table), "where");
				$table = $_table;

				if ($pos)
				{
					if (substr($_table, $pos - 1, 1) == ' ' && substr($table, $pos + 5, 1) == ' ')
					{
						$table = substr_replace($table, "where", $pos, 5);
						$table = String::Split($table, "where");

						if (isset($table[1]))
						{
							$fields = "{$fields} WHERE {$table[1]}";
						}

						$table = $table[0];
					}
				}

				return "UPDATE {$table} SET {$fields}";
			}
		}
		else
		{
			return null;
		}
	}

	public static function Delete($_table = null, $_where = null)
	{
		if ($_table)
		{
			$sql = "DELETE FROM {$_table}";

			if ($_where)
			{
				$sql .= " WHERE {$_where}";
			}

			return $sql;
		}
		else
		{
			return null;
		}
	}

	public static function Interpret($_input = null)
	{
		$action = null;
		$fields = array();
		$table = null;
		$sql = trim($_input);
		$sql = String::Upper($sql);
		$v = preg_split("/[\s,]+/", $sql);
		$pos_action = array_search("SELECT", $v);
		$pos_table = array_search("FROM", $v);

		if ($pos_table)
		{
			if (isset($v[$pos_table + 1]))
			{
				$table = $v[$pos_table + 1];
			}
		}

		print_r($v);

		return array(
			"action" => $action,
			"fields" => $fields,
			"table" => $table);
	}
}