<?php

/**
* @package	Model
* @version	2.1.2
* @author	DavidBeru
* @since	2013-08-29
* @see		2015-04-29
*/

class Model
{
	private static $regex = '/@\{model:[a-zA-Z0-9@{}. ,_-]{1,255}\}/i';
	private static $string = null;
	private static $class = null;
	private static $method = null;
	private static $args = array();
	private static $status = false;
	private static $gcl = false;

	public static function Get($_name = null, $_type = null)
	{
		$types = array("datatable", "datalist", "select");

		if (in_array($_type, $types, true))
		{
			return self::Interpret($_name, $_type);
		}
		else
		{
			return self::$status = 0;
		}
	}

	private static function Interpret($_input = null, $_type = null)
	{
		if (is_null($_input))
		{
			return null;
		}
		else
		{
			$control = preg_replace_callback(self::$regex, function($matches)
			{
				$val = null;

				foreach ($matches as $key => $value)
				{
					if (preg_match(self::$regex, $value))
					{
						self::$gcl = true;
						$value = substr(trim($value), 0, -1);
						$control = String::Split($value, ', ');
						$val = trim(substr($control[0], 8));
						$object = String::Split($val, '.');
						self::$class = $object[0];
						self::$method = $object[1];
						$args = array();

						if (isset($control[1]))
						{
							for ($i = 1; $i < count($control); $i++)
							{
								$args[($i - 1)] = trim($control[$i]);
							}
						}

						self::$args = $args;
					}
				}

				return $val;

			}, $_input);

			if (self::$gcl == true)
			{
				switch ($_type)
				{
					case "datatable":

						$type = "DataTable";

						break;
					case "datalist":

						$type = "DataList";

						break;
					case "select":

						$type = "Select";

						break;
				}

				$class = self::$class;
				$method = $type . self::$method;
				$args = self::$args;

				if (class_exists($class))
				{
					$object = new $class;

					if (method_exists($object, $method))
					{
						self::$string = $object->$method();
						self::$args = $args;
						self::$status = array(
							"control" => self::$string,
							"args" => self::$args);

						return self::$status;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
}