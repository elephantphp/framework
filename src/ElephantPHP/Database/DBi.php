<?php

/**
* @package	DBi
* @version	4.8
* @author	DavidBeru
* @since	2014-05-28
* @see		2015-12-30
*/

class DBi
{
	private $databases = null;
	private $error_code = 300;
	private $error_message = null;
	private $mysqli = null;
	private $type = null;
	private $sql = null;
	private $db = null;
	private $rows = 0;
	private $result = true;
	private $id = 0;
	private $error = false;
	private $error_no = 0;
	private $ok = true;

	public function __construct()
	{
		$this->databases = ElephantPHP::GetAppConfig("databases");
	}

	public function __call($_name, $_arguments)
	{
		$this->Exception(0, "The method {$_name} no is valid");
	}

	public function __tostring()
	{
		return "DBi";
	}

	public function __destruct()
	{
		if ($this->error === false)
		{
			$this->mysqli->close();
		}
	}

	private function Exception($_code, $_message)
	{
		$this->error = true;

		Response::Error(
			$this->error_code + $_code,
			"DBi: " . $_message,
			null,
			null,
			ELEPHANTPHP_LINK_DOC_ERRORS);
	}

	public function Connect($_db = null)
	{
		$this->db = $_db;

		return $this;
	}

	public function UseDataBase($_db = null)
	{
		$this->db = $_db;

		return $this;
	}

	public function Result()
	{
		switch ($this->type)
		{
			case "SELECT":

				return mysqli_fetch_assoc($this->result);

				break;
			case "QUERY":
			case "INSERT":
			case "UPDATE":
			case "DELETE":

				return $this->result;

				break;
			default:

				return null;

				break;
		}
	}

	public function Results($_key = null)
	{
		switch ($this->type)
		{
			case "SELECT":

				if ($this->Count() === 0)
				{
					return array();
				}
				else
				{
					$return = array();

					if (is_string($_key))
					{
						while ($field = mysqli_fetch_assoc($this->result))
						{
							if (isset($field[$_key]))
							{
								$return[$field[$_key]] = $field;
							}
							else
							{
								break;

								return "The key no exists in the array";
							}
						}
					}
					else
					{
						while ($field = mysqli_fetch_assoc($this->result))
						{
							$return[] = $field;
						}
					}

					return $return;
				}

				break;
			case "QUERY":
			case "INSERT":
			case "UPDATE":
			case "DELETE":

				return $this->result;

				break;
			default:

				return null;

				break;
		}
	}

	public function OK()
	{
		return $this->ok;
	}

	public function Error()
	{
		return $this->error;
	}

	public function ErrorNo()
	{
		return $this->error_no;
	}

	public function SQL()
	{
		return $this->sql;
	}

	public function Rows()
	{
		return $this->rows;
	}

	public function Count()
	{
		return $this->rows;
	}

	public function Id()
	{
		return $this->id;
	}

	public function Query($_sql = null, $_db = null)
	{
		$this->type = "QUERY";
		$this->db = is_null($this->db) ? $_db : $this->db;
		$this->sql = $_sql;

		return $this->Execute();
	}

	public function Select($_sql = null, $_db = null)
	{
		$this->type = "SELECT";
		$this->db = is_null($this->db) ? $_db : $this->db;
		$this->sql = SQL::Select($_sql);

		return $this->Execute();
	}

	public function Update($_sql = null, $_table = null, $_db = null)
	{
		$this->type = "UPDATE";
		$this->db = is_null($this->db) ? $_db : $this->db;
		$fields = is_array($_table) ? $_table : $_sql;
		$table = is_string($_sql) ? $_sql : $_table;
		$this->sql = SQL::Update($fields, $table);
		#$this->sql = is_array($_sql) ? SQL::Update($_sql, $_table) : $_sql;

		return $this->Execute();
	}

	public function UpdateNoNull($_sql = null, $_table = null, $_db = null)
	{
		$this->type = "UPDATE";
		$this->db = is_null($this->db) ? $_db : $this->db;
		$fields = is_array($_table) ? $_table : $_sql;
		$table = is_string($_sql) ? $_sql : $_table;
		$this->sql = SQL::Update($fields, $table, true);
		#$this->sql = is_array($_sql) ? SQL::Update($_sql, $_table, true) : $_sql;

		return $this->Execute();
	}

	public function Insert($_sql = null, $_table = null, $_db = null)
	{
		$this->type = "INSERT";
		$this->db = is_null($this->db) ? $_db : $this->db;
		$fields = is_array($_table) ? $_table : $_sql;
		$table = is_string($_sql) ? $_sql : $_table;
		$this->sql = SQL::Insert($fields, $table);

		return $this->Execute();
	}

	public function Delete($_sql = null, $_db = null)
	{
		$this->type = "DELETE";
		$this->db = is_null($this->db) ? $_db : $this->db;
		$this->sql = $_sql;

		return $this->Execute();
	}

	public function Execute()
	{
		$app_db = $this->databases;
		$diver = isset($app_db["driver"]) ? $app_db["driver"] : "mysql";
		$charset = isset($app_db["charset"]) ? $app_db["charset"] : "UTF8";
		$prefix = isset($app_db["prefix"]) ? $app_db["prefix"] : null;
		$default = isset($app_db["default"]) ? $app_db["default"] : null;
		$databases = isset($app_db["databases"]) ? $app_db["databases"] : null;
		$host = isset($app_db["host"]) ? $app_db["host"] : "localhost";
		$usr = isset($app_db["usr"]) ? $app_db["usr"] : "root";
		$pwd = isset($app_db["pwd"]) ? $app_db["pwd"] : null;
		$db = isset($app_db["db"]) ? $app_db["db"] : null;
		$conn = $this->db;
		$conn = is_null($default) ? $conn : $default;

		if ($conn == "@{default}" && is_null($default))
		{
			$this->Exception(1, "empty default");
		}
		else if (is_null($conn))
		{
			if (empty($db))
			{
				$this->Exception(2, "empty db");
			}
			else
			{
				$this->mysqli = @new mysqli($host, $usr, $pwd, $db);
			}
		}
		else if (is_array($conn))
		{
			if (empty($conn))
			{
				$this->Exception(3, "empty conn");
			}
			else
			{
				$host = isset($conn["host"]) ? $conn["host"] : $host;
				$usr = isset($conn["usr"]) ? $conn["usr"] : $usr;
				$pwd = isset($conn["pwd"]) ? $conn["pwd"] : $pwd;
				$db = isset($conn["db"]) ? $conn["db"] : $db;
				$this->mysqli = @new mysqli($host, $usr, $pwd, $db);
			}
		}
		else
		{
			if (is_null($databases))
			{
				$this->Exception(4, "DataBases is null");
			}
			else if (empty($databases))
			{
				$this->Exception(5, "DataBases is empty");
			}
			else if (is_array($databases))
			{
				if (array_key_exists($conn, $databases))
				{
					$conn = $databases[$conn];
					$host = isset($conn["host"]) ? $conn["host"] : $host;
					$usr = isset($conn["usr"]) ? $conn["usr"] : $usr;
					$pwd = isset($conn["pwd"]) ? $conn["pwd"] : $pwd;
					$db = isset($conn["db"]) ? $conn["db"] : $db;
				}
				else
				{
					$db = $conn;
				}

				$this->mysqli = @new mysqli($host, $usr, $pwd, $prefix . $db);
			}
		}

		if (is_null($this->mysqli))
		{
			throw new Exception("Error in DB", $this->error_code);
		}
		else
		{
			if ($this->mysqli->connect_error)
			{
				$this->Exception($this->mysqli->connect_errno, utf8_encode($this->mysqli->connect_error));
			}
			else
			{
				if (is_null($this->sql))
				{
					$this->Exception(6, "no sql");
				}
				else
				{
					@$this->mysqli->set_charset($charset);
					@$this->sql = Request::Interpret($this->sql);
					$query = @$this->mysqli->query($this->sql);

					if ($query)
					{
						$this->rows = $this->mysqli->affected_rows;
						$this->error = $this->mysqli->error;
						$this->error_no = $this->mysqli->sqlstate;
						$this->ok = ($this->mysqli->sqlstate === "00000") ? true : false;

						switch ($this->type)
						{
							case "INSERT":

								$this->id = $this->mysqli->insert_id;

								break;
							case "SELECT":

								$this->result = $query;

								break;
							case "UPDATE":
								#
								break;
							case "DELETE":
								#
								break;
							default:
								#
								break;
						}
					}
					else
					{
						$this->result = false;
					}

					try
					{
						return $this;
					}
					catch (Exception $e)
					{
						throw new Exception(utf8_encode($e->getMessage()), $this->error_code);
					}
				}
			}
		}
	}
}