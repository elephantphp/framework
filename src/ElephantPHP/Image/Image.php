<?php

/**
* @package	Image
* @version	3.7
* @author	DavidBeru
* @since	2013-08-10
* @see		2016-01-11
*/

class Image
{
	private static $storage = null;
	private static $media = ELEPHANTPHP_STORAGE_MEDIA;
	private static $ext = array("png", "jpeg", "jpg", "gif");

	public function __construct($_storage = null)
	{
		self::$storage = $_storage;
	}

	public static function StorageDisk($_storage = null, $_base = true)
	{
		$storage = Storage::Disk($_storage, $_base);

		return new Image($storage);
	}

	public static function Make($_width = 0, $_height = 0, $_message = null)
	{
		$image = ImageCreate($_width, $_height);
		$colorText = ImageColorAllocate($image, 255, 255, 0);
		$colorBack = ImageColorAllocate($image, 255, 0, 0);
		ImageFill($image, 0, 0, $colorBack);
		ImageString($image, 5, 50, 10, $_message, $colorText);
		Header("Content-type: image/png");
		ImagePNG($image);
		ImageDestroy($image);
	}

	public static function Resize($_file = null, $_width = 100, $_height = null, $_prefix = null)
	{
		if (empty($_file))
		{
			return null;
		}
		else
		{
			$file = File::Get($_file, self::$storage);

			if (is_object($file))
			{
				$dir = $file->Dir();
				$type = $file->Type();
				$name = $file->Name();
				$path = $file->Path();
				$ext = $file->Ext();

				if (in_array($ext, self::$ext))
				{
					switch ($ext)
					{
						case "png":
							$image = ImageCreateFromPNG($path);
							break;
						case "jpeg":
						case "jpg":
							$image = ImageCreateFromJPEG($path);
							break;
						case "gif":
							$image = ImageCreateFromGIF($path);
							break;
					}

					$width = ImageSX($image);
					$height = ImageSY($image);
					$_width = (empty($_width)) ? $width : $_width;
					$_height = (empty($_height)) ? $height : $_height;
					$x_ratio = $_width / $width;
					$y_ratio = $_height / $height;

					if (($width <= $_width) && ($height <= $_height))
					{
						$new_width = $width;
						$new_height = $height;
					}
					else if (($x_ratio * $height) < $_height)
					{
						$new_height = ceil($x_ratio * $height);
						$new_width = $_width;
					}
					else
					{
						$new_width = ceil($y_ratio * $width);
						$new_height = $_height;
					}

					$new_width = round($new_width);
					$new_height = round($new_height);
					$tmp_image = ImageCreateTrueColor($new_width, $new_height);

					ImageCopyResampled(
						$tmp_image,
						$image,
						0,
						0,
						0,
						0,
						$new_width,
						$new_height,
						$width,
						$height);

					$prefix = empty($_prefix) ? "{$new_width}x{$new_height}" : $_prefix;
					$name = "{$name}_resize_{$prefix}";
					$filename = "{$name}.{$ext}";
					$route = "{$dir}/{$filename}";

					switch ($ext)
					{
						case "png":
							ImagePNG($tmp_image, $route, 95);
							break;
						case "jpeg":
						case "jpg":
							ImageJPEG($tmp_image, $route, 95);
							break;
						case "gif":
							ImageGIF($tmp_image, $route, 95);
							break;
					}

					ImageDestroy($tmp_image);

					return array(

						"type" => $type,
						"file" => $filename,
						"name" => $name,
						"ext" => $ext,
						"path" => $route

					);
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}

	public static function Thumbnail($_file = null, $_width = 100, $_prefix = null, $_x = 0, $_y = 0)
	{
		if (empty($_file))
		{
			return null;
		}
		else
		{
			$file = File::Get($_file, self::$storage);

			if (is_object($file))
			{
				$dir = $file->Dir();
				$type = $file->Type();
				$name = $file->Name();
				$path = $file->Path();
				$ext = $file->Ext();

				if (in_array($ext, self::$ext))
				{
					switch ($ext)
					{
						case "png":
							$image = ImageCreateFromPNG($path);
							break;
						case "jpeg":
						case "jpg":
							$image = ImageCreateFromJPEG($path);
							break;
						case "gif":
							$image = ImageCreateFromGIF($path);
							break;
					}

					$width = ImageSX($image);
					$height = ImageSY($image);
					$pos_x = 0;
					$pos_y = 0;

					if ($width > $height)
					{
						$new_width = round(($width * $_width) / $height);
						$new_height = $_width;
						$pos_x = round(($width - $height) / 2) + $_x;
					}
					else
					{
						$new_width = $_width;
						$new_height = ($height * $_width) / $width;
						$pos_y = round(($height - $width) / 2) + $_y;
					}

					$tmp_image = ImageCreateTrueColor($_width, $_width);

					ImageCopyResampled(
						$tmp_image,
						$image,
						0,
						0,
						$pos_x,
						$pos_y,
						$new_width,
						$new_height,
						$width,
						$height);

					$prefix = empty($_prefix) ? $_width : $_prefix;
					$name = "{$name}_thumbnail_{$prefix}";
					$filename = "{$name}.{$ext}";
					$route = "{$dir}/{$filename}";

					switch ($ext)
					{
						case "png":
							ImagePNG($tmp_image, $route, 95);
							break;
						case "jpeg":
						case "jpg":
							ImageJPEG($tmp_image, $route, 95);
							break;
						case "gif":
							ImageGIF($tmp_image, $route, 95);
							break;
					}

					ImageDestroy($tmp_image);

					return array(

						"type" => $type,
						"file" => $filename,
						"name" => $name,
						"ext" => $ext,
						"path" => $route

					);
				}
				else
				{
					return false;
				}
			}
			else
			{
				return $file;
			}
		}
	}

	public static function GetThumbnail($_image = null, $_prefix = null)
	{
		if ($_image)
		{
			$split = String::Split($_image, '.');
			$count = count($split);
			$ext = $split[$count - 1];
			$image = String::Split($_image, ".{$ext}");

			return $image[0] . "_thumbnail_{$_prefix}." . $image[1] . $ext;
		}
		else
		{
			return $_image;
		}
	}

	public static function GetResize($_image = null, $_prefix = null)
	{
		if ($_image)
		{
			$split = String::Split($_image, '.');
			$count = count($split);
			$ext = $split[$count - 1];
			$image = String::Split($_image, ".{$ext}");

			return $image[0] . "_resize_{$_prefix}." . $image[1] . $ext;
		}
		else
		{
			return $_image;
		}
	}

	public static function GetMetadata($_image = null)
	{
		$image = ELEPHANTPHP_STORAGE_MEDIA . $_image;

		if (file_exists($image))
		{
			$exif = exif_read_data($image, 'IFD0');

			if ($exif === false)
			{
				return false;
			}
			else
			{
				$exif = exif_read_data($image, 0, true);
				$metadata = array();

				foreach ($exif as $clave => $sección)
				{
					foreach ($sección as $nombre => $valor)
					{
						$metadata[$clave][$nombre] = $valor;
					}
				}

				return $metadata;
			}
		}
		else
		{
			return "No found the image";
		}
	}
}