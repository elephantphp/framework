<?php

/**
* @package	Cache
* @version	2.4
* @author	DavidBeru
* @since	2014-07-05
* @see		2015-02-08
*/

class Cache
{
	private static $storage = "cache";
	private static $expire = 3;

	public static function Make($_route = null, $_content = null)
	{
		$route = Hash::Make($_route);

		return FMS::Make("{$route}.cache", $_content, self::$storage);
	}

	public static function Get($_route = null)
	{
		if (!$_route)
		{
			return null;
		}
		else
		{
			$route = Hash::Make($_route);
			$file = File::Get("{$route}.cache", "cache");

			if ($file)
			{
				if (time() > ($file->Time() + 60 * 60 * self::$expire))
				{
					return ($file) ? unserialize($file) : $file;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return false;
			}
		}
	}

	public static function Delete($_route = null)
	{
		$route = Hash::Make($_route);

		return File::Delete("{$route}.cache", "cache");
	}

	public static function Flush()
	{
		$files = glob(ELEPHANTPHP_STORAGE . "cache/*.cache", GLOB_NOSORT);

		foreach ($files as $file)
		{
			@unlink($file);
		}

		return true;
	}
}