<?php

/**
* @package	Request
* @version	4.9.1
* @author	DavidBeru
* @since	2013-07-24
* @see		2016-02-12
*/

class Request
{
	private static $regex_request_post = '/@\{request.post.[a-zA-Z_-]{1,255}\}/i';
	private static $regex_request_get = '/@\{request.get.[a-zA-Z_-]{1,255}\}/i';

	public static function Protocol()
	{
		if (isset($_SERVER["REQUEST_SCHEME"]))
		{
			return String::Lower($_SERVER["REQUEST_SCHEME"]);
		}
		else
		{
			$protocol = explode('/', $_SERVER["SERVER_PROTOCOL"]);
			$protocol = isset($protocol[0]) ? $protocol[0] : null;

			return String::Lower($protocol);
		}
	}

	public static function AJAX()
	{
		return !empty($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER["HTTP_X_REQUESTED_WITH"]) == "xmlhttprequest";
	}

	public static function Secure()
	{
		return (self::Protocol() == "https") ? true : false;
	}

	public static function Post($_key = null, $_default = null)
	{
		if (is_null($_key))
		{
			return empty($_POST) ? null : $_POST;
		}
		else
		{
			$value = ArrayManager::Get($_POST, $_key, $_default);
			$value = is_string($value) ? trim($value) : $value;

			if (is_array($value))
			{
				return $value;
			}
			else
			{
				if (Validator::Int($value))
				{
					return $value;
				}
				else
				{
					return empty($value) ? $_default : html_entity_decode($value, ENT_QUOTES);
				}
			}
		}
	}

	public static function Get($_key = null, $_default = null)
	{
		if (is_null($_key))
		{
			return empty($_GET) ? null : $_GET;
		}
		else
		{
			$value = ArrayManager::Get($_GET, $_key, $_default);
			$value = is_string($value) ? trim($value) : $value;

			if (is_array($value))
			{
				return $value;
			}
			else
			{
				if (Validator::Int($value))
				{
					return $value;
				}
				else
				{
					return empty($value) ? $_default : html_entity_decode($value, ENT_QUOTES);
				}
			}
		}
	}

	public static function File($_key = null)
	{
		if (is_null($_key))
		{
			return empty($_FILES) ? null : $_FILES;
		}
		else if (is_string($_key))
		{
			if (isset($_FILES[$_key]))
			{
				$type = $_FILES[$_key]["type"];
				$ext = File::GetExt($_FILES[$_key]["name"]);
				$size = $_FILES[$_key]["size"];
				$tmp = $_FILES[$_key]["tmp_name"];
				$filename = $_FILES[$_key]["name"];
				$name = substr($filename, 0, - (strlen($ext) + 1));
				$error = $_FILES[$_key]["error"];

				switch ($error)
				{
					case 0:
						$message = null;
						break;
					case 1:
						$message = null;
						break;
					case 2:
						$message = null;
						break;
					case 3:
						$message = null;
						break;
					case 4:
						$message = null;
						break;
					case 5:
						$message = null;
						break;
					case 6:
						$message = null;
						break;
					case 7:
						$message = null;
						break;
					case 8:
						$message = null;
						break;
					default:
						$message = null;
						break;
				}

				return array(

					"key" => $_key,
					"tmp" => $tmp,
					"error" => $error,
					"message" => $message,
					"type" => $type,
					"ext" => $ext,
					"size" => $size,
					"filename" => $filename,
					"name" => $name

				);
			}
			else
			{
				return null;
			}
		}
	}

	public static function Params()
	{
		return array(

			"post" => is_array(self::Post()) ? self::Post() : array(),
			"get" => is_array(self::Get()) ? self::Get() : array(),
			"file" => is_array(self::File()) ? self::File() : array()

		);
	}

	public static function Method($_key = null, $_default = null)
	{
		if (is_null($_key))
		{
			return self::Server("REQUEST_METHOD");
		}
		else
		{
			return self::Post($_key, self::Get($_key, $_default));
		}
	}

	public static function IP()
	{
		return self::Server("REMOTE_ADDR");
	}

	public static function Accept()
	{
		return self::Server("HTTP_ACCEPT");
	}

	public static function Lang()
	{
		$language = self::Server("HTTP_ACCEPT_LANGUAGE");

		if (is_null($language))
		{
			$lang = String::Split($language, ',');
			$lang = ArrayManager::Get($lang, 0, ELEPHANTPHP_APP_LANG);
		}
		else
		{
			$lang = ELEPHANTPHP_APP_LANG;
		}

		return $lang;
	}

	public static function Server($_key = null)
	{
		if (is_null($_key))
		{
			return $_SERVER;
		}
		else
		{
			return ArrayManager::Get($_SERVER, $_key, null);
		}
	}

	public static function UserAgent()
	{
		return self::Server("HTTP_USER_AGENT");
	}

	public static function Browser($_key = null)
	{
		$info = String::Lower($_key);
		$browser = get_browser(null, true);

		switch ($_key)
		{
			case "so":
				return $browser["platform"];
				break;
			case "browser":
			case "cssversion":
			case "parent":
			case "cookies":
				return $browser[$info];
				break;
			default:
				return $browser;
				break;
		}
	}

	public static function Make($_url = null, $_data = array(), $_file = null)
	{
		if (empty($_url))
		{
			return null;
		}
		else
		{
			$header = "Content-Type: application/x-www-form-urlencoded";
			$content = null;
			$boundary = Hash::Make();
			$data = ($_data === true) ? self::Post() : $_data;
			$file = ($_file === true) ? self::File() : $_file;
			$data = is_array($data) ? $data : null;
			$file = is_array($file) ? $file : null;

			if (is_array($data) && is_null($file))
			{
				$content = http_build_query($data);
			}
			else
			{
				if (is_array($data))
				{
					$content .= self::ArrayFormData($boundary, $data);
				}

				if (is_array($file))
				{
					$header = "Content-Type: multipart/form-data; boundary=\"{$boundary}\";";

					foreach ($file as $key => $value)
					{
						$value = self::File($key);

						if (is_array($value))
						{
							$content .= "--{$boundary}\n";
							$content .= "Content-Disposition: form-data; name=\"{$key}\"; filename=\"{$value["filename"]}\";\n";
							$content .= "Content-Type: {$value["type"]};\n";
							$content .= "Content-Transfer-Encoding: binary;\n\n";
							$content .= file_get_contents($value["tmp"]) . "\n";
						}
					}
				}

				$content .= "--{$boundary}--\n";
			}

			$context = stream_context_create(array(

				"http" => array(

					"method" => "POST",
					"header" => $header,
					"content" => $content

				)

			));
			$contents = @file_get_contents(urldecode($_url), false, $context);

			return ($contents === false) ? false : $contents;
		}
	}

	public static function CURL($_url = null, $_header = null, $_fields = null)
	{
		$header = is_null($_header) ? null : $_header;

		if (is_array($header))
		{
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $_url);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $_header);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_POSTFIELDS, JSON::Make($_fields));
			$result = curl_exec($curl);
			curl_close($curl);

			return $result;
		}
		else
		{
			return null;
		}
	}

	public static function ArrayFormData($_boundary, $_data, $_content = null, $_name = null)
	{
		foreach ($_data as $key => $value)
		{
			if (is_array($value))
			{
				$name = $_name ? "{$_name}[$key]" : $key;
				$_content = self::ArrayFormData($_boundary, $value, $_content, $name);
			}
			else
			{
				$name = $_name ? "{$_name}[$key]" : $key;
				$_content .= "--{$_boundary}\r\n";
				$_content .= "Content-Disposition: form-data; name=\"{$name}\";\n\n{$value}\n";
			}
		}

		return $_content;
	}

	public static function Interpret($_string)
	{
		$search = array(
			"@{post.ecv}",
			"@{get.ecv}");
		$replace = array(
			"@{request.post.elephant_control_value}",
			"@{request.get.elephant_control_value}");
		$string = str_replace(
			$search,
			$replace,
			$_string);

		$string = preg_replace_callback(self::$regex_request_post, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_request_post, $value))
				{
					$val = self::Post(substr($value, 15, -1));
				}
			}

			return $val;

		}, $string);

		$string = preg_replace_callback(self::$regex_request_get, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_request_get, $value))
				{
					$val = self::Get(substr($value, 14, -1));
				}
			}

			return $val;

		}, $string);

		return $string;
	}
}