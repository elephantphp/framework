<?php

/**
* @package	RPC
* @version	1.1
* @author	DavidBeru
* @since	2015-12-17
* @see		2015-12-31
*/

class RPC
{
	# Vars API
	private $code = 200;
	private $message = "OK";
	private $connected = false;
	private $response = [];
	private $response_options = null;

	public function __destruct()
	{
		if (Request::Get("pretty") == true)
		{
			$this->response_options = JSON_PRETTY_PRINT;
		}

		Response::JSON([

			"meta" => [

				"code" => $this->code,
				"message" => $this->message

			],
			"response" => $this->response

		], $this->response_options);
	}

	protected function Meta($_code = null, $_message = null)
	{
		$this->code = $_code;
		$this->message = $_message;
	}

	protected function Code($_code = null)
	{
		if (is_null($_code))
		{
			return $this->code;
		}
		else
		{
			$this->code = $_code;
		}
	}

	protected function Message($_message = null)
	{
		if (is_null($_message))
		{
			return $this->message;
		}
		else
		{
			$this->message = $_message;
		}
	}

	protected function Connected($_connected = null)
	{
		if (is_null($_connected))
		{
			return $this->connected;
		}
		else if (is_bool($_connected))
		{
			$this->connected = $_connected;
		}
	}

	protected function Response($_response = null)
	{
		if (is_null($_response))
		{
			return $this->response;
		}
		else
		{
			$this->response = $_response;
		}
	}
}