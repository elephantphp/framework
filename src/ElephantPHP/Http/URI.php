<?php

/**
* @package	URI
* @version	4.4.0
* @author	DavidBeru
* @since	2013-06-24
* @see		2016-02-05
*/

class URI
{
	private static $level = ELEPHANTPHP_APP_LEVEL;
	private static $auth_uri = ELEPHANTPHP_APP_AUTH;
	private static $regex_uri = '/@\{[0-9]{1,2}\}/i';
	private static $regex_uri_current = '/@\{url.current\}/i';
	private static $regex_uri_home = '/@\{url.home\}/i';
	private static $regex_uri_controller = '/@\{url.home.[a-zA-Z_\/.-]{1,255}\}/i';

	public static function Domain()
	{
		return $_SERVER["SERVER_NAME"];
	}

	public static function Base()
	{
		$base = null;
		$uri = self::Domain() . $_SERVER["REQUEST_URI"];
		$count = count($uri);
		$split = String::Split($uri, '/');

		for ($i = 0; $i <= self::$level; $i++)
		{
			$base .= $split[$i] . '/';
		}

		return $base;
	}

	public static function Current()
	{
		return Request::Protocol() . "://" . self::Domain() . $_SERVER["REQUEST_URI"];
	}

	public static function Path($_start = 0)
	{
		return self::Split($_SERVER["REQUEST_URI"], $_start);
	}

	public static function Home($_path = null, $_parameters = array(), $_identifier = null)
	{
		$base = self::Base();

		if (ELEPHANTPHP_APP_SERVER === "IIS")
		{
			$base .= "index.php/";
		}

		return Request::Protocol() . "://" . $base . self::Parameters(
			$_path,
			$_parameters,
			$_identifier);
	}

	public static function Themes($_path = null)
	{
		return Request::Protocol() . "://" . self::Base() . "themes/" . $_path;
	}

	public static function ElephantPHP($_path = null)
	{
		return self::Home("elephantphp/" . $_path);
	}

	public static function Media($_path = null)
	{
		return self::ElephantPHP("media/" . $_path);
	}

	public static function ElephantPHPAssets($_path = null)
	{
		return self::ElephantPHP("assets/" . $_path);
	}

	public static function ElephantJS()
	{
		return self::ElephantPHPAssets("js/elephantphp.js");
	}

	public static function Auth($_parameters = array(), $_identifier = null)
	{
		return URI::Parameters(self::$auth_uri, $_parameters, $_identifier);
	}

	public static function Segment($_input = 0)
	{
		$split = String::Split(self::Path(), '?');
		$split = String::Split($split[0], '/');
		$segment = empty($split[$_input]) ? null : $split[$_input];

		return isset($split[$_input]) ? $segment : null;
	}

	public static function Parameters($_url = null, $_parameters = array(), $_identifier = null)
	{
		$url = $_url;

		if (is_array($_parameters) && !empty($_parameters))
		{
			$symbol = (count(String::Split($_url, '?')) > 0) ? '?' : '&';

			foreach ($_parameters as $key => $value)
			{
				$url .= "{$symbol}{$key}={$value}";
				$symbol = '&';
			}
		}

		return self::Interpret(is_string($_identifier) ? "{$url}#{$_identifier}" : $url);
	}

	public static function Detect($string = null)
	{
		if ($string)
		{
			$string = html_entity_decode($string);
			$string = " " . $string;
			$string = @eregi_replace('(((f|ht) {1}tp://)[-a-zA-Z0-9@:%_+.~#?&//=]+)', ' <a href="\1" target="_blank">\1</a> ', $string);
			$string = @eregi_replace('(((f|ht) {1}tps://)[-a-zA-Z0-9@:%_+.~#?&//=]+)', ' <a href="\1" target="_blank">\1</a> ', $string);
			$string = @eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_+.~#?&//=]+)', '\1 <a href="http://\2" target="_blank">\2</a> ', $string);
			$string = @eregi_replace('([_.0-9a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,3})', '<a href="mailto:\1" target="_blank">\1</a> ', $string);

			return $string;
		}
	}

	public static function GetLevel($_input = null)
	{
		$split = String::Split($_input, '/');

		return count($split);
	}

	public static function Split($_input = null, $_start = 0)
	{
		$_input = substr($_input, 1);
		$url = String::Split($_input, '?');
		$url = String::Split($url[0], '/');
		$c = count($url);
		$path = null;

		if (ELEPHANTPHP_APP_SERVER === "IIS")
		{
			$level = self::$level + 1;
		}
		else
		{
			$level = self::$level;
		}

		$start = Validator::Int($_start) ? $_start + $level : $level;

		for ($i = $start; $i <= $c; $i++)
		{
			$u = isset($url[$i]) ? $url[$i] : null;

			if ($u)
			{
				$n = explode('?', $url[$i]);
				$path .= empty($n[0]) ? null : $n[0] . '/';
			}
		}

		return is_null($path) ? '/' : substr($path, 0, -1);
	}

	public static function BreadCrumbs($_level = 0)
	{
		$base[] = URI::Base();
		$path = String::Split(self::Path(), '/');
		$url = (self::Path() == '/') ? $base : array_merge($base, $path);
		$dir = null;
		$menu = array();
		$level = is_integer($_level) ? $_level : 0;
		$i = 0;

		foreach ($url as $key => $value)
		{
			if ($value == self::Base())
			{
				$dir .= $value;
				$v = String::Split(substr($value, 0, -1), '/');
				$c = count($v);
				$val = $v[$c - 1];
			}
			else
			{
				$dir .= $value;
				$val = $value;
			}

			if ($i > $level)
			{
				$menu["http://{$dir}"] = ucwords(self::ClearString($val));
			}
			else
			{
				$i++;
			}

			if ($value != self::Base())
			{
				$dir .= '/';
			}
		}

		return $menu;
	}

	public static function ErrorLog()
	{
		$path = self::Path();
		$path = ($path == '/') ? "index" : $path;

		return Date::YMD() . ' ' . str_replace('/', ' ', String::Lower($path));
	}

	public static function ClearString($_string = null)
	{
		$find = array('-', '_');
		$replace = array(' ', ' ');
		$string = str_replace($find, $replace, $_string);
		$ext = explode('.', $string);

		return isset($ext[1]) ? $ext[0] : $string;
	}

	public static function Slug($_input = null)
	{
		if (empty($_input))
		{
			return null;
		}
		else
		{
			$allowed = array(

				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
				'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
				's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ',
				0, 1, 2, 3, 4, 5, 6, 7, 8, 9, '-', '.', '/'

			);

			$str = String::ClearAccents($_input);
			$str = String::Lower($str);
			$tmp_str = null;

			foreach (str_split(trim($str)) as $key => $value)
			{
				$value = Validator::Int($value) ? (int) $value : $value;

				if (in_array($value, $allowed, true) === true)
				{
					$tmp_str .= $value;
				}
			}

			$str = str_replace('.', '', $tmp_str);
			$str = str_replace(' ', '-', $str);
			$str = str_replace('/', '-', $str);
			$str = str_replace("--", '-', $str);
			$str = str_replace("--", '-', $str);

			return $str;
		}
	}

	public static function Encode($_url = null)
	{
		return is_string($_url) ? urlencode($_url) : $_url;
	}

	public static function Decode($_url = null)
	{
		return is_string($_url) ? urldecode($_url) : $_url;
	}

	public static function Interpret($_input = null)
	{
		$convert = preg_replace_callback(self::$regex_uri, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_uri, $value))
				{
					$val = substr($value, 2, -1);
				}
			}

			return self::Segment($val);

		}, $_input);

		$convert = preg_replace_callback(self::$regex_uri_home, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_uri_home, $value))
				{
					$val = self::Home();
				}
			}

			return $val;

		}, $convert);

		$convert = preg_replace_callback(self::$regex_uri_current, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_uri_current, $value))
				{
					$val = self::Current();
				}
			}

			return $val;

		}, $convert);

		$convert = preg_replace_callback(self::$regex_uri_controller, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_uri_controller, $value))
				{
					$val = self::Home(substr($value, 11, -1));
				}
			}

			return $val;

		}, $convert);

		return $convert;
	}
}