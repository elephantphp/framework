<?php

/**
* @package	Elephant
* @version	4.11.0
* @author	DavidBeru
* @since	2013-07-21
* @see		2016-01-24
*/

class ElephantPHP
{
	private static $key = null;
	private static $class = null;
	private static $method = null;
	private static $method_args = array();
	private static $controller = null;
	private static $controller_path = null;
	private static $controller_name = null;
	private static $controller_url = null;
	private static $controller_level = 0;
	private static $controller_args = array();
	private static $resources = array();
	private static $args = array();
	private static $config = array();
	private static $libraries = array();
	private static $routes = array();
	private static $exception_code = 0;
	private static $include = array();
	private static $buffer = null;

	private static function SetUp()
	{
		# Reserved Words
		define("ELEPHANT", "elephantphp");
		define("ELEPHANTPHP", "elephantphp");

		# Paths
		define("ELEPHANTPHP_BASE", dirname(__FILE__) . "/../../");
		define("ELEPHANTPHP_CONTROLLERS", ELEPHANTPHP_BASE . "elephant/controllers/");
		define("ELEPHANTPHP_VIEWS", ELEPHANTPHP_BASE . "elephant/views/");
		define("ELEPHANTPHP_STORAGE", ELEPHANTPHP_BASE . "storage/");
		define("ELEPHANTPHP_STORAGE_MEDIA", ELEPHANTPHP_STORAGE . "media/");
		define("ELEPHANTPHP_APP_CONTROLLERS", ELEPHANTPHP_BASE . "app/controllers/");

		# Settings
		self::$config = self::GetAppConfig("config");
		$config = self::$config;
		$level = isset($config["level"]) ? $config["level"] : 0;
		$develop = isset($config["develop"]) ? $config["develop"] : true;
		$down = isset($config["down"]) ? $config["down"] : false;
		$server = isset($config["server"]) ? $config["server"] : null;
		$server = is_string($server) ? $server : null;
		$timezone = isset($config["timezone"]) ? $config["timezone"] : null;
		$lang = isset($config["lang"]) ? $config["lang"] : "en";
		$auth = isset($config["auth"]) ? $config["auth"] : null;
		$session_length = isset($config["session_length"]) ? $config["session_length"] : 3600;
		$session_length = is_integer($session_length) ? $session_length : 3600;
		$user_session_length = isset($config["user_session_length"]) ? $config["user_session_length"] : 3600;
		$user_session_length = is_integer($user_session_length) ? $user_session_length : 3600;
		$cookie_domain = isset($config["cookie_domain"]) ? $config["cookie_domain"] : null;
		$cookie_length = isset($config["cookie_length"]) ? $config["cookie_length"] : 3600;
		$cookie_length = is_integer($cookie_length) ? $cookie_length : 3600;

		# ElephantPHP
		define("ELEPHANTPHP_LINK_DOC_ERRORS", ELEPHANTPHP_LINK . "doc/Errors#");

		# App
		define("ELEPHANTPHP_APP_LEVEL", $level);
		define("ELEPHANTPHP_APP_DEVELOP", $develop);
		define("ELEPHANTPHP_APP_DOWN", $down);
		define("ELEPHANTPHP_APP_SERVER", $server);
		define("ELEPHANTPHP_APP_TIMEZONE", $timezone);
		define("ELEPHANTPHP_APP_LANG", $lang);
		define("ELEPHANTPHP_APP_AUTH", $auth);
		define("ELEPHANTPHP_APP_SESSION_LENGTH", $session_length);
		define("ELEPHANTPHP_APP_USER_SESSION_LENGTH", $user_session_length);
		define("ELEPHANTPHP_APP_COOKIE_DOMAIN", $cookie_domain);
		define("ELEPHANTPHP_APP_COOKIE_LENGTH", $cookie_length);

		# PHP INI
		ini_set("date.timezone", ELEPHANTPHP_APP_TIMEZONE);
		ini_set("display_errors", 0);
		ini_set("error_log", ELEPHANTPHP_STORAGE . "logs/" . URI::ErrorLog() . ".log");

		# Errors
		error_reporting(E_ALL);
		register_shutdown_function("ElephantPHP::FatalHandler");
	}

	private static function IncludeLibraries()
	{
		$libraries = self::GetAppConfig("libraries");

		if (is_array($libraries))
		{
			self::$libraries = $libraries;

			foreach (self::$libraries as $key => $value)
			{
				$path = ELEPHANTPHP_BASE . "/libraries/{$value}";

				if (file_exists($path) && is_file($path))
				{
					require_once $path;
				}
			}
		}
	}

	private static function Resources($resource = null)
	{
		$open = opendir($resource);

		if ($open)
		{
			while (false !== ($element = readdir($open)))
			{
				$extension = substr($element, -3);

				if ($extension === "php")
				{
					$path = ELEPHANTPHP_BASE . "/{$resource}/{$element}";

					if (file_exists($path) && is_file($path))
					{
						self::$resources[] = $path;
						require_once $path;
					}
				}
			}

			closedir($open);
		}
	}

	private static function Similar($_level = 0)
	{
		$path = URI::Path($_level);
		$count = 0;
		$similar = '/';
		$controller_args = array();
		$method_args = array();
		$url = String::Split($path, '/');

		if ($path)
		{
			foreach (self::$routes as $key => $class)
			{
				$i = 0;
				$tmp_count = 0;
				$tmp_similar = "";
				$tmp_controller_args = array();
				$tmp_method_args = array();
				$tmp_route = null;
				$tmp_key = null;
				$tmp_level = 0;

				foreach (String::Split($key, '/') as $sub_key => $sub_value)
				{
					if ($sub_value == "{%}" || $sub_value == "{@}" || $sub_value == "{?}")
					{
						$segment = URI::Segment($tmp_level);

						if ($sub_value == "{@}")
						{
							$tmp_controller_args[] = $segment;
						}

						if ($sub_value == '{?}')
						{
							$tmp_method_args[] = $segment;
						}

						$segment = is_null($segment) ? $sub_value : $segment;
					}
					else
					{
						$segment = $sub_value;
					}

					$u = isset($url[$i]) ? $url[$i] : null;

					if (String::Lower($segment) == String::Lower($u))
					{
						self::$controller_url .= "{$segment}/";
						$i++;
						$tmp_count++;
						$tmp_similar .= "{$sub_value}/";
					}

					$tmp_route .= "{$segment}/";
					$tmp_key .= "{$sub_value}/";
					$tmp_level++;
				}

				$tmp_route = String::Lower(substr($tmp_route, 0, -1));
				$tmp_key = substr($tmp_key, 0, -1);

				if ($tmp_count > $count)
				{
					$count = $tmp_count;
					$similar = $tmp_similar;
					$controller_args = $tmp_controller_args;
					$method_args = $tmp_method_args;
				}
			}

			self::$controller_url = substr(self::$controller_url, 0, -1);
			self::$controller_args = $controller_args;
			self::$method_args = $method_args;
			$similar = substr($similar, 0, -1);
		}

		return array_key_exists($similar, self::$routes) ? $similar : 404;
	}

	private static function MultiLevel($_controller = null, $_method = null)
	{
		if (Validator::Int($_method))
		{
			$path = URI::Path($_method);
			$split = String::Split($path, '/');
			$tmp = null;
			$tmp_method = null;

			foreach ($split as $key => $value)
			{
				$tmp_method .= "{$value}";
				$method = str_replace('/', '__', $tmp_method);
				$method = str_replace('-', '_', $method);

				if (method_exists($_controller, $method))
				{
					$tmp = "{$method}/";
				}

				$tmp_method .= '/';
			}

			if (is_null($tmp))
			{
				$method = URI::Segment($_method);
				$method = is_null($method) ? 'index' : String::Lower($method);
			}
			else
			{
				$tmp = substr($tmp, 0, -1);
				$method = str_replace('/', '__', $tmp);
			}
		}
		else
		{
			$method = $_method;
		}

		$method = str_replace('-', '_', $method);

		return $method;
	}

	private static function ControllerExists($_controller = null, $_method = 0, $_ec = false)
	{
		$path_controller = ($_ec === true) ? ELEPHANTPHP_CONTROLLERS : ELEPHANTPHP_APP_CONTROLLERS;

		# Colon
		$split_colon = String::Split($_controller, ':');

		if (count($split_colon) > 1)
		{
			$path = $split_colon[0] . '/';
			$controller = $split_colon[1];
		}
		else
		{
			$path = null;
			$controller = $split_colon[0];
		}

		self::$controller_path = $path;

		# Hash
		$split_hash = String::Split($controller, '#');

		if (count($split_hash) > 1)
		{
			self::$controller_level = $_method;
			$controller = $split_hash[0];
			$method = $split_hash[1];
		}
		else
		{
			self::$controller_level = $_method + 1;
			$controller = $split_hash[0];
			$method = $_method;
		}

		$path_controller .= "{$path}{$controller}.php";

		if (file_exists($path_controller))
		{
			@require_once $path_controller;

			if (class_exists($controller))
			{
				$method = self::MultiLevel($controller, $method);
				self::$controller = $controller;
				self::$controller_url .= "/{$method}";
				self::$method = $method;

				if (method_exists($controller, $method))
				{
					$path = URI::Path(self::$controller_level);
					$split = String::Split($path, '/');
					self::$method_args = array_merge(self::$method_args, $split);

					return $method;
				}
				else
				{
					return 2;
				}
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	private static function Controller($_ec = false)
	{
		if (isset(self::$routes['/']))
		{
			$controller = self::$routes['/'];
			$level = ($_ec === true) ? 1 : 0;
			$home = self::ControllerExists($controller, $level, $_ec);

			if (!is_string($home))
			{
				$similar = self::Similar($level);

				if ($similar === 404)
				{
					$controller = $controller;
					$method = $level;
				}
				else
				{
					$controller = self::$routes[$similar];
					$level = ($_ec === true) ? 1 : 0;
					$method = URI::GetLevel($similar) + $level;
				}

				$home = self::ControllerExists($controller, $method, $_ec);
			}

			if ($home === 0)
			{
				Response::Error(500, Lang::Get("@{elephantphp}.errors.10000", array(

					"controller" => $controller

				)));
			}
			else if ($home === 1)
			{
				Response::Error(500, Lang::Get("@{elephantphp}.errors.10001", array(

					"controller" => $controller

				)));
			}
			else if ($home === 2)
			{
				Response::Error(404, Lang::Get("@{elephantphp}.errors.10002", array(

					"controller" => $controller,
					"method" => self::$method

				)));
			}
			else if ($home === 404)
			{
				Response::Error(404, Lang::Get("@{elephantphp}.errors.10003"));
			}
			else
			{
				ob_start("ElephantPHP::Buffer");
				$class = new ReflectionClass(self::$controller);
				$instance = $class->newInstanceArgs(self::$controller_args);
				call_user_func_array(array($instance, $home), self::$method_args);
			}
		}
		else
		{
			Response::Error(500, Lang::Get("@{elephantphp}.errors.10004"));
		}
	}

	public static function Buffer($_buffer)
	{
		self::$buffer = $_buffer;

		return ob_get_contents();
	}

	public static function FatalHandler()
	{
		#Session::Put("elephantphp.last-movement", Date::DateTime()->OutPut());
		$error = error_get_last();

		if (is_array($error))
		{
			Response::Error(
				$error["type"],
				$error["message"],
				$error["file"],
				$error["line"],
				ELEPHANTPHP_LINK_DOC_ERRORS);
		}
		else
		{
			# No error
		}
	}

	private static function FrontController()
	{
		$module = String::Lower(URI::Segment(0));

		if ($module === "app" || $module === "storage" || $module === "themes")
		{
			Response::Error(403, "Access Denied");
		}
		else if ($module === "elephant")
		{
			Redirect::Home("elephantphp");
		}
		else if ($module === "elephantphp")
		{
			self::$routes = self::GetElephantPHPConfig("routes");
			self::Controller(true);
		}
		else
		{
			self::$routes = self::GetAppConfig("routes");
			self::Controller();
		}
	}

	private static function GetFile($_route = null, $_key = null)
	{
		if (is_string($_route))
		{
			$route = ELEPHANTPHP_BASE . $_route;

			if (file_exists($route))
			{
				ob_start();
				$content = require $route;
				ob_get_clean();

				if (is_array($content))
				{
					if (is_null($_key))
					{
						return $content;
					}
					else
					{
						return ArrayManager::Get($content, $_key);
					}
				}
				else
				{
					return null;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	private static function Autoload($_module = null)
	{
		$module = strtolower($_module);

		switch ($module)
		{
			case "app":
				$module = "ElephantPHP/App";
				break;
			case "arraymanager":
				$module = "ElephantPHP/Functions/ArrayManager";
				break;
			case "barcode":
				$module = "ElephantPHP/Image/Barcode";
				break;
			case "cache":
				$module = "ElephantPHP/Http/Cache";
				break;
			case "cookie":
				$module = "ElephantPHP/Http/Cookie";
				break;
			case "csv":
				$module = "ElephantPHP/File/CSV";
				break;
			case "datalist":
				$module = "ElephantPHP/Html/DataList";
				break;
			case "datatable":
				$module = "ElephantPHP/Html/DataTable";
				break;
			case "date":
				$module = "ElephantPHP/Date/Date";
				break;
			case "db":
				$module = "ElephantPHP/Database/DB";
				break;
			case "dbi":
				$module = "ElephantPHP/Database/DBi";
				break;
			case "file":
				$module = "ElephantPHP/Storage/File";
				break;
			case "fms":
				$module = "ElephantPHP/Storage/FMS";
				break;
			case "fn":
				$module = "ElephantPHP/Functions/Fn";
				break;
			case "folder":
				$module = "ElephantPHP/Storage/Folder";
				break;
			case "form":
				$module = "ElephantPHP/Html/Form";
				break;
			case "ftp":
				$module = "ElephantPHP/Providers/FTP";
				break;
			case "hash":
				$module = "ElephantPHP/Security/Hash";
				break;
			case "html":
				$module = "ElephantPHP/Html/HTML";
				break;
			case "htmli":
				$module = "ElephantPHP/Html/HTMLi";
				break;
			case "image":
				$module = "ElephantPHP/Image/Image";
				break;
			case "json":
				$module = "ElephantPHP/Http/JSON";
				break;
			case "lang":
				$module = "ElephantPHP/Translation/Lang";
				break;
			case "mail":
				$module = "ElephantPHP/Providers/Mail";
				break;
			case "modal":
				$module = "ElephantPHP/Html/Modal";
				break;
			case "model":
				$module = "ElephantPHP/Database/Model";
				break;
			case "paginator":
				$module = "ElephantPHP/Html/Paginator";
				break;
			case "pdf":
				$module = "ElephantPHP/File/PDF";
				break;
			case "qrcode":
				$module = "ElephantPHP/Image/QRCode";
				break;
			case "redirect":
				$module = "ElephantPHP/Http/Redirect";
				break;
			case "request":
				$module = "ElephantPHP/Http/Request";
				break;
			case "response":
				$module = "ElephantPHP/Http/Response";
				break;
			case "rpc":
				$module = "ElephantPHP/Http/RPC";
				break;
			case "session":
				$module = "ElephantPHP/Storage/Session";
				break;
			case "simpledb":
				$module = "ElephantPHP/Database/SimpleDB";
				break;
			case "simpledbi":
				$module = "ElephantPHP/Database/SimpleDBi";
				break;
			case "socket":
				$module = "ElephantPHP/Providers/Socket";
				break;
			case "sql":
				$module = "ElephantPHP/Database/SQL";
				break;
			case "storage":
				$module = "ElephantPHP/Storage/Storage";
				break;
			case "storagei":
				$module = "ElephantPHP/Storage/Storagei";
				break;
			case "string":
				$module = "ElephantPHP/Functions/String";
				break;
			case "uri":
				$module = "ElephantPHP/Http/URI";
				break;
			case "user":
				$module = "ElephantPHP/Auth/User";
				break;
			case "validator":
				$module = "ElephantPHP/Validation/Validator";
				break;
			case "view":
				$module = "ElephantPHP/View/View";
				break;
			case "gcm":
				$module = "ElephantPHP/Providers/GCM";
				break;
			case "websocket":
				$module = "ElephantPHP/Providers/WebSocket";
				break;
			default:

				$dirname = dirname(__FILE__);
				$model = $dirname . "/../../app/models/{$_module}.php";
				$rule = $dirname . "/../../app/rules/{$_module}.php";

				if (file_exists($model))
				{
					require_once $model;

					return true;
				}
				else if (file_exists($rule))
				{
					require_once $rule;

					return true;
				}
				else
				{
					$module = $_module;
				}

				break;
		}

		$path = dirname(__FILE__) . "/{$module}.php";

		if (file_exists($path))
		{
			require_once $path;

			return true;
		}
		else
		{
			#throw @new Exception("Error autoload {$module}");
		}
	}

	/**
	* @category Public
	*/

	public static function GetAppConfig($_file = null, $_key = null)
	{
		return self::GetFile("app/config/{$_file}.php", $_key);
	}

	public static function GetElephantPHPConfig($_file = null, $_key = null)
	{
		return self::GetFile("elephant/config/{$_file}.php", $_key);
	}

	public static function Run()
	{
		try
		{
			# Autoload
			spl_autoload_register(function($_class = null)
			{
				self::Autoload($_class);
			});

			# Setup
			self::SetUp();
			$back_to = User::BackTo();

			if (!is_null($back_to) && User::Auth())
			{
				User::BackTo(true);
				Redirect::Home($back_to);
			}
			else
			{
				Session::Start();
				self::IncludeLibraries();
				self::FrontController();
			}
		}
		catch (Exception $e)
		{
			Response::Error(
				$e->getCode(),
				$e->getMessage(),
				$e->getFile(),
				$e->getLine());
		}
	}

	public static function GetController()
	{
		return self::$controller;
	}

	public static function GetControllerName()
	{
		return self::$controller;
	}

	public static function GetControllerPath()
	{
		return self::$controller_path;
	}

	public static function GetMethod()
	{
		return self::$method;
	}

	public static function GetMethodName()
	{
		return self::$method;
	}

	public static function GetControllerURL()
	{
		return self::$controller_url;
	}

	public static function GetResources()
	{
		return self::$resources;
	}
}