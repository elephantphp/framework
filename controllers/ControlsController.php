<?php

/**
* @package	Controls
* @version	1.0.0
* @author	DavidBeru
* @since	2016-02-05
* @see		2016-02-05
*/

class ControlsController
{
	private $control = null;
	private $name = null;

	public function __construct()
	{
		$this->control = @hex2bin(Request::Post("elephant_control", Request::Get("elephant_control")));
		$this->name = Request::Post("elephant_control_name", Request::Get("elephant_control_name"));
	}

	public function Index(){}

	public function DataList()
	{
		$control = Model::Get($this->control, "datalist");

		if ($control)
		{
			Response::Send(DataList::Get(
				$this->name,
				$this->control,
				Fn::Args($control["control"]["sql"], $control["args"]),
				$control["control"]["fields"],
				$control["control"]["db"]));
		}
		else
		{
			Response::Send("No control found");
		}
	}

	public function DataTable()
	{
		$control = Model::Get($this->control, "datatable");

		if ($control)
		{
			Response::Send(DataTable::Get(
				$this->name,
				$this->control,
				$control["control"],
				$control["args"]));
		}
		else
		{
			Response::Send("No control found");
		}
	}

	public function DataTable__CSV()
	{
		$control = Model::Get($this->control, "datatable");

		if ($control)
		{
			$data = DataTable::Get(
				$this->name,
				$this->control,
				$control["control"],
				$control["args"],
				true);

			$csv = [];
			$csv = array_merge($csv, [$data["columns"]]);
			$csv = array_merge($csv, $data["values"]);

			Response::CSV($csv, $this->name);
		}
		else
		{
			Response::Send("No control found");
		}
	}

	public function MediaUpload()
	{
		$save_on = Request::Post("save_on");
		$save_as = Request::Post("save_as");
		$save_on = ($save_on == "undefined") ? null : $save_on;
		$save_as = ($save_as == "undefined") ? null : $save_as;

		if (empty($_FILES))
		{
			Response::JSON(array("status" => false));
		}
		else
		{
			$files = array();

			foreach ($_FILES as $key => $value)
			{
				$save_as = ($save_as == "@{hash}") ? Hash::Make() : $save_as;
				$files[] = File::Upload($key, $save_on, $save_as);
			}

			Response::JSON(array(

				"status" => true,
				"files" => $files

			));
		}
	}
}