<?php

/**
* @package	Home
* @version	2.1.0
* @author	DavidBeru
* @since	2014-07-25
* @see		2016-02-05
*/

class Home
{
	public function Index()
	{
		Response::View("@{base}.elephant.views.hello");
	}

	public function Info()
	{
		Response::JSON(array(

			"author" => ELEPHANTPHP_AUTHOR,
			"name" => ELEPHANTPHP_NAME,
			"date" => ELEPHANTPHP_DATE,
			"version" => ELEPHANTPHP_VERSION

		));
	}

	public function Media()
	{
		Response::File(URI::Path(2));
	}

	public function Assets()
	{
		Response::File(URI::Path(2), null, "../elephant/assets");
	}

	public function ControlsOff()
	{
		$cl = @hex2bin(Request::Post("elephant_control"));
		$name = Request::Post("elephant_control_name");
		$type = URI::Segment(2);

		switch ($type)
		{
			case "datalist":

				$control = Model::Get($cl, "datalist");

				if ($control)
				{
					Response::Send(DataList::Get(
						$name,
						$cl,
						Fn::Args($control["control"]["sql"], $control["args"]),
						$control["control"]["fields"],
						$control["control"]["db"]));
				}
				else
				{
					Response::Send("No control found");
				}

				break;
			case "datatable":

				$control = Model::Get($cl, "datatable");

				if ($control)
				{
					Response::Send(DataTable::Get(
						$name,
						$cl,
						$control["control"],
						$control["args"]));
				}
				else
				{
					Response::Send("No control found");
				}

				break;
			case "mediaupload":

				$save_on = Request::Post("save_on");
				$save_as = Request::Post("save_as");
				$save_on = ($save_on == "undefined") ? null : $save_on;
				$save_as = ($save_as == "undefined") ? null : $save_as;

				if (empty($_FILES))
				{
					Response::JSON(array("status" => false));
				}
				else
				{
					$files = array();

					foreach ($_FILES as $key => $value)
					{
						$save_as = ($save_as == "@{hash}") ? Hash::Make() : $save_as;
						$files[] = File::Upload($key, $save_on, $save_as);
					}

					Response::JSON(array(
						"status" => true,
						"files" => $files));
				}

				break;
		}
	}

	public function LangCode()
	{
		Response::View("@{base}.elephant.views.langcode");
	}

	public function PHPInfo()
	{
		phpinfo();
	}
}