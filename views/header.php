<ul>
	<li class="link">
		<a href="<?php echo URI::Home(); ?>">GO TO HOME</a>
	</li>
	<li class="link version" title="Current Version">
		<a><?php echo ELEPHANTPHP_VERSION; ?></a>
	</li>
	<li class="link" title="Go to elephantphp.com">
		<a href="<?php echo ELEPHANTPHP_LINK; ?>" target="_blank"><?php echo ELEPHANTPHP_NAME; ?></a>
	</li>
</ul>