<div class="datatable">
	<?php echo Form::Hidden("datatable-url-{$name}", URI::Home("elephantphp/controls/datatable")); ?>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="<?php echo $name; ?>">Buscar</label>
				<?php echo Form::Text($name, "")->Attr(array(

					"data-fw-control-type" => "datatable-search",
					"data-fw-control" => $control,
					"data-fw-control-name" => $name,
					"placeholder" => $search,
					"class" => "form-control"

				)); ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="<?php echo $name; ?>">Resultados por página</label>
				<?php echo Form::Select("datatable_limit_" . $name, array(

					5 => 5,
					25 => 25,
					50 => 50,
					100 => 100,
					500 => 500

				), 5)->Attr(array(

					"data-fw-control-type" => "datatable-limit",
					"data-fw-control" => $control,
					"data-fw-control-name" => $name,
					"class" => "form-control"

				)); ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="<?php echo $name; ?>">Acciones</label>
				<div>
					<div class="btn-group btn-group-justified">
						<?php if (@$new === true) : ?>
						<div class="btn-group" role="group">
							<?php echo Form::Button("dt-{$name}-btn-new", '+')->Attr("class", "btn btn-primary"); ?>
						</div>
						<?php endif; ?>
						<div class="btn-group" role="group">
							<?php echo Form::Button("dt-btn-{$name}-reload", "Reload")->Attr("class", "btn"); ?>
						</div>
						<div class="btn-group" role="group">
							<?php echo Form::Button("dt-btn-{$name}-csv", "CSV")->Attr("class", "btn")->Attr([

								"data-fw-control-type" => "datatable-csv",
								"data-fw-control" => $control,
								"data-fw-control-name" => $name

							]); ?>
						</div>
						<div class="btn-group" role="group">
							<?php echo Form::Button("dt-btn-{$name}-print", "Print")->Attr("class", "btn"); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="datatable-table-<?php echo $name;?>"><?php echo @$table; ?></div>
</div>