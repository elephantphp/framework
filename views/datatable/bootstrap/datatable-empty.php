<?php

echo HTML::Open("h3", "No hay datos que mostrar.")->Attr("class", "text text-center text-margin");

if ($new === true)
{
	$icon = HTML::Open("i")->Attr("class", "glyphicon glyphicon-plus");
	echo HTML::Open("button", $icon)->Attr([

		"name" => "dt-{$name}-btn-new",
		"id" => "dt-{$name}-btn-new",
		"class" => "btn btn-primary btn-lg btn-new"

	]);
}