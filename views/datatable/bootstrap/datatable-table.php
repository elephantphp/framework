<?php

$name = $elephant_cl["name"];
$rows = $elephant_cl["rows"];
$rows_all = $elephant_cl["rows_all"];
$control = $elephant_cl["control"];
$limit = $elephant_cl["limit"];
$page = $elephant_cl["page"];
$columns = $elephant_cl["columns"];
$values = $elephant_cl["values"];
$actions = $elephant_cl["actions"];
$delete = $elephant_cl["delete"];

if (empty($values))
{
	echo "<h4 class=\"text text-center text-margin\">Hay un error en la consulta :(</h4>";
}
else
{

?>
<table id="<?php echo $name; ?>" class="table">
	<thead>
		<tr>
			<?php

			echo "<th>#</th>";

			if (!empty($actions))
			{
				echo "<th></th>";
			}

			foreach ($columns as $key => $value)
			{
				echo "<th>$value</th>";
			}

			if ($delete == true)
			{
				echo "<th></th>";
			}

			?>
		</tr>
	</thead>
	<tbody>
		<?php

		$n = $page;

		foreach ($values as $key => $value)
		{
			$n++;
			$html = "<tr data-primary=\"" . $key . "\">";
			$html .= "<td>" . $n . "</td>";
			
			if (!empty($actions))
			{
				$count = count($actions);

				$html .= "<td>";

				if ($count > 1)
				{
					$html .= "<div class=\"btn-group btn-group-xs\">";
				}

				foreach ($actions as $txt => $btn)
				{
					$html .= Form::Button("dt-{$name}-btn-{$btn}", $txt)->Data("value", $key)->Attr("class", "btn btn-xs");
				}

				if ($count > 1)
				{
					$html .= "</div>";
				}

				$html .= "</td>";
			}

			for ($i = 0; $i < count($value); $i++)
			{ 
				$html .= "<td>" . Fn::Markup($value[$i], Request::Post("elephant_control_value")) . "</td>";
			}

			if ($delete == true)
			{
				$html .= "<td>";
				$html .= Form::Button("dt-btn-{$name}-delete", "X", array("data-value" => $key, "class" => "btn btn-mini btn-delete"));
				$html .= "</td>";
			}

			$html .= "</tr>";

			echo $html;
		}

		?>
	</tbody>
</table>
<div class="row">
	<div class="col-md-8">
		<strong>
			<?php

			$del = ($page + 1);
			$al = ($rows + $page);

			echo "Mostrando del $del al $al de $rows_all resultados.";

			?>
		</strong>
	</div>
	<div class="col-md-4">
		<?php echo Paginator::Ajax($control, $name, $rows_all, $limit); ?>
	</div>
</div>
<?php

}

?>