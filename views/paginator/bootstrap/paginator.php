<?php

$first = null;
$before = null;
$after = null;
$last = null;

# First
if ($page > 2)
{
	$btn = Form::Button("", "Primero")
		->Data("value", 1)
		->Attr("class", "btn")
		->Title("Ir a la primera página");
	$first = HTML::Open("div", $btn)
		->Attr("role", "group")
		->Attr("class", "btn-group");
}

# Before
if ($page > 1)
{
	$btn = Form::Button("", "Anterior")
		->Data("value", $prev)
		->Attr("class", "btn")
		->Title("Página anterior");
	$before = HTML::Open("div", $btn)
		->Attr("role", "group")
		->Attr("class", "btn-group");
}

# After
if ($page < $total)
{
	$btn = Form::Button("", "Siguiente")
		->Data("value", $next)
		->Attr("class", "btn")
		->Title("Página siguiente");
	$after = HTML::Open("div", $btn)
		->Attr("role", "group")
		->Attr("class", "btn-group");
}

# Last
if ($page < ($total - 1))
{
	$btn = Form::Button("", "Último")
		->Data("value", $total)
		->Attr("class", "btn")
		->Title("Ir a la ultima página");
	$last = HTML::Open("div", $btn)
		->Attr("role", "group")
		->Attr("class", "btn-group");
}

echo HTML::Open("div", $first . $before . $after . $last)
	->Data("fw-control-type", "datatable-paginator")
	->Data("fw-control-name", $name)
	->Data("fw-control", $control)
	->Data("fw-perpage", $perpage)
	->Attr("role", "group")
	->Attr("class", "btn-group btn-group-justified");

?>