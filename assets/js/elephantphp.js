/**
* @package hummngbird JS
* @author Dhomper
* @version 1.0.1
*/

var js_name_loader = "hummngbird-panel-loader";
var js_name_modal = "hummngbird-panel-modal";
var js_name_alert = "hummngbird-panel-alert";
var js_text_loading = "Loading...";
var elephantphp_dt_search = "";
var elephantphp_dt_limit = 5;
var elephantphp_dt_page = 1;

$(document).ready(function()
{
	$("body").append("<span id=\"" + js_name_loader + "\"></span>");
	$("body").append("<span id=\"" + js_name_modal + "\"></span>");
	$("body").append("<span id=\"" + js_name_alert + "\"></span>");

	$(document).on("click", "ul.nav > li", function()
	{
		$(this).parent("ul.nav").find("> li").removeClass("active");
		$(this).addClass("active");
	});

	$(document).on("click", "ul.nav > li ul li", function()
	{
		$(this).parent("ul.nav li ul").find("li").removeClass("active");
		$(this).addClass("active");
	});

	$(document).on("click", ".modal-close", function()
	{
		$.Modal();
	});

	$(document).on("click", ".alert-remove", function()
	{
		$(".alert-remove").remove(".alert-remove");
	});

	JS.Navbar();
	JS.Steps();

	$(document).on("keyup", "input", function()
	{
		if ($(this).data("fw-control-type") == "datalist")
		{
			var control = $(this).data("fw-control");
			var name = $(this).data("fw-control-name");
			var val = $(this).val();

			if (val == '')
			{
				$("#datalist_" + name).html('');
			}
			else
			{
				$.post(url_home + "elephantphp/controls/datalist",
				{
					elephant_control: control,
					elephant_control_name: name,
					elephant_control_value: val

				}, function(data)
				{
					$("#datalist_" + name).html(data);
				});
			}
		}
	});

	$(document).on("click", "li", function()
	{
		if ($(this).data("fw-type") == "datalist")
		{
			var val = $(this).data("fw-value");
			var txt = $(this).text();
			var name = $(this).data("fw-name");

			$('#' + name).val(val);
			$("#datalist_text_" + name).val(txt);
			$("#datalist_" + name).html('');
		}
	});

	$(document).on("keyup", "input[data-fw-control-type=datatable-search]", function()
	{
		var value = $(this).val();
		var control = $(this).data("fw-control");
		var name = $(this).data("fw-control-name");
		var url = $("#datatable-url-" + name).val();
		elephantphp_dt_search = value;

		Request.Ajax(url, "#datatable-table-" + name,
		{
			elephant_control: control,
			elephant_control_name: name,
			elephant_control_value: elephantphp_dt_search,
			elephant_control_limit: elephantphp_dt_limit
		});
	});

	var page = 1;

	$(document).on("click", "div[data-fw-control-type=datatable-paginator] button", function()
	{
		page = $(this).data("value");
		elephantphp_dt_page = page;
	});

	$(document).on("click", "div[data-fw-control-type=datatable-paginator]", function()
	{
		var value = page;
		var control = $(this).data("fw-control");
		var name = $(this).data("fw-control-name");
		var url = $("#datatable-url-" + name).val();

		Request.Ajax(url, "#datatable-table-" + name,
		{
			elephant_control: control,
			elephant_control_name: name,
			elephant_control_value: elephantphp_dt_search,
			elephant_control_limit: elephantphp_dt_limit,
			elephant_control_page: elephantphp_dt_page
		});
	});

	$(document).on("click", "button[data-fw-control-type=datatable-csv]", function()
	{
		var control = $(this).data("fw-control");
		var name = $(this).data("fw-control-name");
		var url = $("#datatable-url-" + name).val();

		window.location.href = url + "/csv"
			+ "?elephant_control=" + control
			+ "&elephant_control_name=" + name
			+ "&elephant_control_value=" + elephantphp_dt_search
			+ "&elephant_control_limit=" + elephantphp_dt_limit
			+ "&elephant_control_page=" + elephantphp_dt_page;
	});

	$(document).on("change", "select[data-fw-control-type=datatable-limit]", function()
	{
		var value = $(this).val();
		var control = $(this).data("fw-control");
		var name = $(this).data("fw-control-name");
		var url = $("#datatable-url-" + name).val();
		elephantphp_dt_limit = value;

		Request.Ajax(url, "#datatable-table-" + name,
		{
			elephant_control: control,
			elephant_control_name: name,
			elephant_control_value: elephantphp_dt_search,
			elephant_control_limit: elephantphp_dt_limit
		});
	});

	$(document).on("change", "[data-fw-control-type=\"mediaupload\"]", function()
	{
		var id = $(this).attr("id");
		var name = $(this).data("fw-control-name");
		var archivos = document.getElementById(id);
		var archivo = archivos.files;
		var formdata = new FormData();
		var multiple = $(this).attr("multiple");
		var autoload = $(this).attr("autoload");

		formdata.append("save_on", $(this).attr("save-on"));
		formdata.append("save_as", $(this).attr("save-as"));

		for (i = 0; i < archivo.length; i++)
		{
			formdata.append("file-" + i, archivo[i]);
		}

		var area_upload = $(".area-upload").html();

		$(".vista-previa").text("Wait Please, Uploading...");

		$.ajax(
		{
			url: "//" + document.domain + "/frame/controls/mediaupload",
			type: "POST",
			contentType: false,
			data: formdata,
			processData: false,
			cache: false

		}).done(function(data)
		{
			if (data.status == true)
			{
				var routes = "";
				var media = "";

				$.each(data.files, function(key, value)
				{
					routes += value["route"] + ',';
					media += value["media"] + ',';
				});

				var end = routes.length;
				var rou = routes.substr(0, (end - 1));

				var end = media.length;
				var med = media.substr(0, (end - 1));

				var med_split = med.split(',');

				$(".vista-previa").text('');

				$('#' + name).val(rou);
				$(".area-upload").html(area_upload);
				$(".area-upload-select").addClass("min");

				var files = ""

				$.each(med_split, function(key, value)
				{
					files += "<img src=\"" + value + "\">";
				});

				$(".vista-previa").html(files);
			}
			else
			{
				alert("Error, ocurrio un problema al cargar la imagen");
			}
		});
	});
});

function FW()
{
	this.Alert = function()
	{

	}
}

FW = new FW();

function Request()
{
	this.Ajax = function(_url, _response, _args)
	{
		//$(_response).html('');

		var post = $.post(_url, _args, function(data)
		{
			if (typeof _response == 'undefined')
			{
				return data;
			}
			else
			{
				$(_response).html(data);
			}
		});

		post.fail(function()
		{
			alert("Error de comunicación al procesar la petición");
		});
	}
}

Request = new Request();

function CheckAlert()
{
	$(document).ready(function()
	{
		if ($('#'+js_name_alert).html())
		{
			JS.TimeOut(function()
			{
				$(".alert").fadeOut("slow", function()
				{
					$('#'+js_name_alert).empty()
				});
			}, 5000);
		}
	});
}

$(document).ajaxStart(function()
{
	JS.LoaderElement();
});

$(document).ajaxComplete(function()
{
	JS.LoaderElement(false);
	CheckAlert();
	JS.Navbar();
	JS.Steps();
});

function JS()
{
	this.EleById = function(_ele_id)
	{
		return document.getElementById(_ele_id);
	}

	this.IconLoader = function(_ele_id)
	{
		return document.getElementById(_ele_id).innerHTML = "<div class=\"loader-element\"></div>";
	}

	this.LoaderElement = function(_type)
	{
		if (_type == false)
		{
			JS.EleById(js_name_loader).innerHTML = "";	
		}
		else
		{
			JS.EleById(js_name_loader).innerHTML = "<div class=\"loader-element\">"+js_text_loading+"</div>";
		}
	}

	this.Modal = function(_data)
	{
		return document.getElementById(js_name_modal).innerHTML = _data;
	}

	this.Put = function(_ele_id, _data)
	{
		JS.IconLoader(_ele_id);
		return document.getElementById(_ele_id).innerHTML = _data;
	}

	this.Flush = function(_ele_id)
	{
		return document.getElementById(_ele_id).innerHTML = "";
	}

	this.PressEnter = function(e, i)
	{
		if (event.keyCode == 13)
		{
			e(i);
		}
	}

	this.AddAttr = function(e, i, o)
	{
		if (o == null)
		{
			o = "true";
		}

		document.getElementById(e).setAttribute(i, o);
	}

	this.RemoveAttr = function(e, i, o)
	{
		if (o == null)
		{
			o = "false";
		}

		document.getElementById(e).removeAttribute(i, o);
	}

	var time_out = true;

	this.Timetrue = function()
	{
		time_out = true;
		clearTimeout(time_out);
	}

	this.TimeOut = function(e, t)
	{
		time_out = setTimeout(e, t);
		setTimeout(function()
		{
			JS.Timetrue();
		}, t+1);
	}

	this.Alert = function(_data, _put)
	{
		if (_put != null)
		{
			$(".alert").remove(".alert");
			$(_put).append(_data);
		}
		else
		{
			JS.EleById(js_name_alert).innerHTML = _data;
		}
	}
	
	this.AlertSuccess = function()
	{
		JS.EleById(js_name_alert).innerHTML = "<div class=\"alert alert-fixed alert-success\">Well done!</div>";
	}

	this.Upload = function(_url, _input)
	{
		var archivos = document.getElementById(_input);
		var archivo = archivos.files;
		var data2 = new FormData();

		for (i = 0; i < archivo.length; i ++)
		{
			data2.append('archivo'+i,archivo[i]);
		}

		var area_upload = document.getElementById('multimedia-area-upload').innerHTML;

		document.getElementById('multimedia-area-upload').innerHTML = '<div class="area-loading">Wait Please, Uploading...</div>';

		$.ajax({
			url: _url,
			type: "POST",
			contentType: false,
			data: data2,
			processData: false,
			cache: false
		}).done(function(msg)
		{
			$("#multimedia-file-upload").html(msg);
			document.getElementById('multimedia-area-upload').innerHTML = area_upload;
		});
	}

	this.Cookie = function(_name, _value, _days)
	{
		var d = new Date();
		d.setTime(d.getTime() + (_days*24*60*60*1000));
		var expires = "expires=" + d.toGMTString();
		document.cookie= _name + '=' + _value + ';' + expires;
	}

	this.Navbar = function()
	{
		$(document).ready(function()
		{
			var bs_tabs = new Array();
			var bs_tabs_name = new Array();
			var bs_tabs_id = $("div[data-js='hummngbird-navbar']").attr("id");
			var bs_tabs_num = -1;

			$("div[data-js='hummngbird-navbar'] .tab").each(function()
			{
				bs_tabs.push($(this).attr("id"));
				bs_tabs_name.push($(this).data("tabname"));
				bs_tabs_num ++;
			});

			if ($("#bs-steps-navbar-"+bs_tabs_id).length == 0)
			{
				$('#'+bs_tabs_id).before("<div id=\"bs-steps-navbar-"+bs_tabs_id+"\"></div>");	
				$("#bs-steps-navbar-"+bs_tabs_id).append("<div class=\"navbar\"></div>");
				$(".navbar").append("<div class=\"navbar-inner\"></div>");
				$(".navbar-inner").append("<ul></ul>");

				var i = 1;
				for (var x in bs_tabs)
				{
					$(".navbar-inner ul").append("<li id=\"bs_"+bs_tabs[x]+"\" class=\"bs-control-id\"><a>"+bs_tabs_name[x]+"</a></li>");
					i ++;

					if (x == 0)
					{
						$('#bs_'+bs_tabs[x]).addClass("active");
					}
					else
					{
						$('#'+bs_tabs[x]).hide();
					}
				}
			}

			$(document).on("click", ".bs-control-id", function()
			{
				for (var x in  bs_tabs)
				{
					if ($(this).attr("id") == "bs_"+bs_tabs[x])
					{
						$('#'+bs_tabs[x]).fadeIn(500);
						$('#bs_'+bs_tabs[x]).addClass("active");
					}
					else
					{
						$('#bs_'+bs_tabs[x]).removeClass("active");
						$('#'+bs_tabs[x]).fadeOut(-700);
					}
				}
			});
		});
	}

	this.Steps = function()
	{
		$(document).ready(function()
		{
			var bs_steps = new Array();
			var bs_steps_name = new Array();
			var bs_steps_num = -1;
			var bs_steps_btnid = new Array();
			var bs_steps_id = $(".hummngbird-steps").attr("id");
			var bs_steps_type = $(".hummngbird-steps").attr("stepstype");

			$(".hummngbird-steps .step").each(function()
			{
				bs_steps.push($(this).attr("id"));
				bs_steps_name.push($(this).attr("stepname"));
				bs_steps_btnid.push($(this).attr("stepbtnid"));
				bs_steps_num ++;
			});

			if (bs_steps_type == "navbar")
			{
				if ($("#bs-steps-navbar-"+bs_steps_id).length == 0)
				{
					$('#'+bs_steps_id).before("<div id=\"bs-steps-navbar-"+bs_steps_id+"\"></div>");	
					$("#bs-steps-navbar-"+bs_steps_id).append("<div class=\"navbar\"></div>");
					$(".navbar").append("<div class=\"navbar-inner\"></div>");
					$(".navbar-inner").append("<ul>");

					var i = 1;
					for (var x in bs_steps)
					{
						$(".navbar-inner ul").append("<li id=\"bs_"+bs_steps[x]+"\" class=\"bs-control-id\"><a>"+bs_steps_name[x]+"</a></li>");
						i ++;

						if (x == 0)
						{
							$('#bs_'+bs_steps[x]).addClass("active");
						}
						else
						{
							$('#'+bs_steps[x]).hide();
						}
					}
				}
			}
			else
			{
				$('#'+bs_steps_id).before("<div class=\"bs-control-steps\"></div>");
				$('#'+bs_steps_id).after("<div class=\"bs-control-steps-actions\"></div>");

				var i = 1;
				for (var x in bs_steps)
				{
					$(".bs-control-steps").append("<div class=\"bs-control-step\" id=\"bs_"+bs_steps[x]+"\" title=\""+bs_steps_name[x]+"\">"+i+"</div>");
					i ++;

					if (x == 0)
					{
						$('#bs_'+bs_steps[x]).addClass("active");
						bs_actions(x);
					}
					else
					{
						$('#'+bs_steps[x]).hide();
					}
				}
			}

			function bs_actions(x)
			{
				$(".bs-control-steps-actions").empty();

				if (x == 0)
				{
					$(".bs-control-steps-actions").append("<button type=\"button\" value=\""+(parseInt(x)+1)+"\" class=\"bs-control-btn btn btn-primary\">Siguiente</button>");
				}
				else if (x == bs_steps_num)
				{
					$(".bs-control-steps-actions").append("<button type=\"button\" value=\""+(parseInt(x)-1)+"\" class=\"bs-control-btn btn btn-primary\">Anterior</button>");
					$(".bs-control-steps-actions").append("<button type=\"button\" id=\""+bs_steps_btnid[x]+"\" value=\""+x+"\" class=\"bs-control-finish btn btn-primary\">Finalizar</button>");
				}
				else
				{
					$(".bs-control-steps-actions").append("<button type=\"button\" value=\""+(parseInt(x)-1)+"\" class=\"bs-control-btn btn btn-primary\">Anterior</button>");
					$(".bs-control-steps-actions").append("<button type=\"button\" value=\""+(parseInt(x)+1)+"\" class=\"bs-control-btn btn btn-primary\">Siguiente</button>");
				}
			}

			function bs_step(i)
			{
				for (var x in  bs_steps)
				{
					if (i == x)
					{
						$('#'+bs_steps[x]).fadeIn(500);
						$('#bs_'+bs_steps[x]).addClass("active");
						bs_actions(x);
					}
					else
					{
						$('#bs_'+bs_steps[x]).removeClass("active");
						$('#'+bs_steps[x]).fadeOut(-700);
					}
				}
			}

			$(document).on("click", ".bs-control-btn", function()
			{
				bs_step($(this).val());
			});

			$(document).on("click", ".bs-control-id", function()
			{
				for (var x in  bs_steps)
				{
					if ($(this).attr("id") == "bs_"+bs_steps[x])
					{
						$('#'+bs_steps[x]).fadeIn(500);
						$('#bs_'+bs_steps[x]).addClass("active");
						bs_actions(x);
					}
					else
					{
						$('#bs_'+bs_steps[x]).removeClass("active");
						$('#'+bs_steps[x]).fadeOut(-700);
					}
				}
			});
		});
	}
}

var JS = new JS();

function Validator()
{
	this.isDate = function(day, month, year)
	{
		month = month - 1;
		dteDate = new Date(year,month,day);
		return ((day == dteDate.getDate()) && (month == dteDate.getMonth()) && (year == dteDate.getFullYear()));
	}

	this.Date = function(_input)
	{
		var patron = new RegExp("^(19|20)+([0-9]{2})([-])([0-9]{1,2})([-])([0-9]{1,2})$");

		if (_input.search(patron) == 0)
		{
			var values = _input.split('-');
			
			if (Validator.isDate(values[2], values[1], values[0]))
			{
				return true;
			}
		}

		return false;
	}
}

var Validator = new Validator();
var modal_is_open = false;
var modal_level = 0;
var modal_temp = new Array();

jQuery.extend(
{
	Modal: function(_data)
	{
		if (typeof(_data) == 'undefined')
		{
			$("body").css("overflow", "auto");
			
			modal_level--;

			if (modal_level > 0)
			{
				$("body").css("overflow", "hidden");
				$('#' + js_name_modal).html(modal_temp[modal_level - 1]);
			}
			else
			{
				$("#modal-backdrop").remove("#modal-backdrop");
				$('#' + js_name_modal).html('');
			}

			modal_temp[modal_level] = null;
		}
		else
		{
			$("body").css("overflow", "hidden");
			$('#' + js_name_modal).html(_data);

			modal_temp[modal_level] = _data;
			modal_level++;
		}
	},

	Alert: function(_data)
	{
		$('#' + js_name_alert).html("<div class=\"alert alert-remove alert-fixed\">" + _data + "</div>");
	},
	
	AlertSuccess: function()
	{
		$('#' + js_name_alert).html("<div class=\"alert alert-remove alert-fixed alert-success\">Well done!</div>");
	},

	OpenWindow: function(_url)
	{
		window.open(_url, "_blank", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=500, height=500, left=50, right=50");
	}
});