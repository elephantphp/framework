<?php

/**
* @package	ElephantPHP
* @version	0.7.10.1
* @author	DavidBeru
* @since	2013-06-24
* @see		2016-02-12
*/

# Framework information
define("ELEPHANTPHP_NAME", "ELEPHANTPHP, PHP FRAMEWORK");
define("ELEPHANTPHP_VERSION", "0.7.10.1");
define("ELEPHANTPHP_AUTHOR", "DAVID BERUMEN (DAVIDBERU) - DHOMPER TECNOLOGIAS");
define("ELEPHANTPHP_DATE", "2016-02-12");
define("ELEPHANTPHP_LINK", "http://elephantphp.com/");

# Elephant
require_once dirname(__FILE__) . "/src/ElephantPHP.php";

# Run
ElephantPHP::Run();